using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceInteractable : Interactable
{
    public Resource Resource;
    public bool destroyWhenEmpty = false;
    public bool grabAllOnInteract = true;

    public int ResourcePerHit;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Interact()
    {
        base.Interact();

        int amount = ResourcePerHit;
        if( grabAllOnInteract )
            amount = Resource.Amount;

        int grab = Mathf.Min( Resource.Amount, amount );

        Resource.Amount -= grab;

        PlayerResources.Instance.AddToStack( Resource.type, grab );

        if ( destroyWhenEmpty && Resource.Amount == 0 )
        {
            Destroy( this.gameObject );
        }

        GameObject hitMarker = Instantiate( UnityEngine.Resources.Load( "HitValue" ) as GameObject );
        hitMarker.transform.position = transform.position + Vector3.up * 1.0f;
        hitMarker.GetComponentInChildren<TextMeshProUGUI>().text = string.Format( "+{0} {1}", amount, System.Enum.GetName( typeof( Resources ), Resource.type ) );
    }
}
