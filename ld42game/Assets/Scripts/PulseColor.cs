﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( 25 )]
public class PulseColor : MonoBehaviour
{
    public MeshRenderer TargetRenderer;
    public float PulseWidth;


    void Start()
    {
    }

    void Update()
    {
        Color baseColor = TargetRenderer.material.color;

        float t = 1.0f + Mathf.PingPong( Time.time * PulseWidth, PulseWidth ) - PulseWidth / 2.0f;

        baseColor.a *= t;

        TargetRenderer.material.color = baseColor;
    }
}
