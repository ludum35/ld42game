using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( 250 )]
public class ResourceBox : ResourceInteractable
{
    public bool useDefaultHitValue = true;
    // Use this for initialization
    void Start()
    {
        if ( PlayerResources.Instance != null )
        {
            this.Resource = PlayerResources.Instance.GetResourceBoxValue( Resource.type );
            if ( useDefaultHitValue )
            {
                ResourcePerHit = PlayerResources.Instance.ResourcePerHit;
            }
        }
    }


}
