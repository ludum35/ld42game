using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : Targetable
{
    public Vector3 mousePos;

    public float MouseSensitivity = 5f;

    public Vector3 additive;
    public Vector3 RotationLimitLower;
    public Vector3 RotationLimitUpper;
    public float distanceMagnitude = 3f;
    public bool useDistanceMagnitude = false;
    void Start()
    {

    }

    public Vector3 ForceLimits( Vector3 RotateTo )
    {
        RotateTo += transform.localEulerAngles;

        for ( int i = 0; i < 3; i++ )
        {
            if ( RotateTo[i] > 180 )
            {
                RotateTo[i] = RotateTo[i] - 360f;
            }

            if ( RotationLimitLower[i] > RotateTo[i] )
            {
                RotateTo[i] = RotationLimitLower[i];
            }
            if ( RotationLimitUpper[i] < RotateTo[i] )
            {
                RotateTo[i] = RotationLimitUpper[i];
            }
        }

        RotateTo -= transform.localEulerAngles;

        return RotateTo;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        mousePos.y = Input.GetAxis( "Mouse X" ) * MouseSensitivity;
        mousePos.x = -Input.GetAxis( "Mouse Y" ) * MouseSensitivity;

        mousePos = ForceLimits( mousePos );
        Vector3 forward = Target.position - transform.position;
        if ( useDistanceMagnitude )
        {
            if ( forward.magnitude > distanceMagnitude )
            {
                transform.position += forward * Time.deltaTime;
            }
        }

        transform.RotateAround( Target.position, Vector3.up, mousePos.y );
        transform.RotateAround( Target.position, transform.right, mousePos.x );

    }
}
