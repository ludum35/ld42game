
using UnityEngine;
using UnityEngine.UI;

public class ResourceUIResourceInstance : MonoBehaviour
{
    public Resource Resource;

    public Text Text;

    public void Update()
    {
        if ( PlayerResources.Instance == null )
        {
            return;
        }
        Resource = PlayerResources.Instance.getStack( Resource.type );
        if ( Resource != null )
        {
            Text.text = Resource.Amount.ToString();
        }
    }
}