﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactorCore : MonoBehaviour
{
    public float WaterS = 1.0f;
    public float OxygenS = 0.25f;
    public float FoodS = 0.1f;
    public StructureConsole Console;

    private float FoodTick = 0;
    private float spawnSlumTimer = 0.0f;

    void Start()
    {
        if( Console != null )
            UpdateConsole();


        WaterFilter.TotalProduction += WaterS;
        Farm.TotalProduction += FoodS;
        AirFilter.TotalProduction += OxygenS;
    }

    private void OnDestroy()
    {
        WaterFilter.TotalProduction -= WaterS;
        Farm.TotalProduction -= FoodS;
        AirFilter.TotalProduction -= OxygenS;
    }

    void FixedUpdate()
    {
        GameController controller = GameController.Instance;

        controller.Water += WaterS * Time.deltaTime;

        controller.OxygenPercentage = Mathf.Min( 100.0f, controller.OxygenPercentage + OxygenS * Time.deltaTime );


        FoodTick += FoodS * Time.deltaTime;
        if( FoodTick > 1.0f )
        {
            FoodTick -= 1.0f;
            controller.Food++;
        }

        spawnSlumTimer += Time.deltaTime;
        if( spawnSlumTimer > 60.0f )
        {

            Structure structure = GetComponent<Structure>();
            Vector2i newSlumTarget;
            bool newTileFound = structure.FindNewExpansionTile( out newSlumTarget );

            if( !newTileFound )
            {
                // TODO: Become angry 
            }
            else
            {
                Structure newSlum = controller.LevelData.CreateBlueprint( StructureType.NewSlum, newSlumTarget );

                spawnSlumTimer -= 60.0f;

                NPCAI[] npcs = structure.GetStructureNPCs();
                if( newSlum != null )
                {
                    int detachAmount = npcs.Length / 2;
                    for( int i = 0; i < detachAmount; ++i )
                    {
                        npcs[i].MoveToNewSlum( newSlum );
                    }
                }
            }
        }

    }

    void UpdateConsole()
    {
        Console.Content = string.Format( "water: {0}/s\noxygen: {1}/s\nfood: {2}/s", WaterS, OxygenS, FoodS );
    }
}
