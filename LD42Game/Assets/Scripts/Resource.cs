using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Resource
{
    public Resources type;
    public int Amount;

    public Resource( Resources type, int amount )
    {
        this.type = type;
        Amount = amount;
    }
}
