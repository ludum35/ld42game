using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct StructureDataList
{
    public List<Structure> Slums;
    public List<Structure> WaterFilters;
    public List<Structure> AirFilters;
    public List<Structure> Farms;
    public List<Structure> LivingSpaces;
    public List<Structure> NewSlums;
    public Structure ReactorCore;
    public List<Structure> Pillars;
    public List<Structure> ResourceSpots;
    public Structure Engine;
    public Structure InfiniteMetal;
    public Structure InfiniteElectronics;
    public Structure InfiniteConcrete;
}

[System.Serializable]
public struct StructureCost
{
    public StructureType type;
    public List<Resource> Costs;
}

[DefaultExecutionOrder( -100 )]
public class LevelData : MonoBehaviour
{
    public Vector2i Size = new Vector2i( 16, 32 );

    public StructureDataList StructurePrefabs;

    public List<StructureCost> StructureCosts;

    public delegate void OnTileChanged( Vector2i tile );


    private LinearMap2D<OnTileChanged> tileEvents;

    private LinearMap2D<Structure> structures;

    void Start()
    {
        structures = new LinearMap2D<Structure>( Size );
        tileEvents = new LinearMap2D<OnTileChanged>( Size );
    }

    public bool IsEmpty( Vector2i position )
    {
        return structures[position] == null;
    }



    public void AddTileEventListener( Vector2i tile, OnTileChanged eventListener )
    {
        if ( tileEvents.IsInside( tile ) )
        {
            tileEvents[tile] += eventListener;
        }
    }

    public void RemoveTileEventListener( Vector2i tile, OnTileChanged eventListener )
    {
        if ( tileEvents.IsInside( tile ) )
        {
            tileEvents[tile] -= eventListener;
        }
    }

    public void RemoveStructure( Vector2i position )
    {
        if ( structures.IsInside( position ) )
        {
            structures[position] = null;

            if ( tileEvents[position] != null )
                tileEvents[position]( position );
        }
    }

    public Structure GetStructure( Vector2i tile )
    {
        return structures[tile];
    }

    public List<Resource> GetStructureCost( StructureType type )
    {
        StructureCost? resources = StructureCosts.Find( ( x ) => x.type == type );
        if ( resources.HasValue )
        {
            return resources.Value.Costs;
        }
        return null;
    }

    public void DoNotUseThis( Vector2i tile, Structure a )
    {
        structures[tile] = a;
    }

    public Structure CreateBlueprint( StructureType type, Vector2i position, float rotation = -1.0f )
    {
        // Check if in bounds and is empty spot
        if ( !structures.IsInside( position ) || structures[position] != null )
            return null;

        GameObject structureObject = null;

        switch( type )
        {
            default:
            case StructureType.Slum:
                structureObject = Instantiate( StructurePrefabs.Slums.Random().gameObject );
                break;
            case StructureType.WaterFilter:
                structureObject = Instantiate( StructurePrefabs.WaterFilters.Random().gameObject );
                break;
            case StructureType.AirFilter:
                structureObject = Instantiate( StructurePrefabs.AirFilters.Random().gameObject );
                break;
            case StructureType.Farm:
                structureObject = Instantiate( StructurePrefabs.Farms.Random().gameObject );
                break;
            case StructureType.LivingSpace:
                structureObject = Instantiate( StructurePrefabs.LivingSpaces.Random().gameObject );
                break;
            case StructureType.NewSlum:
                structureObject = Instantiate( StructurePrefabs.NewSlums.Random().gameObject );
                break;
            case StructureType.ReactorCore:
                structureObject = Instantiate( StructurePrefabs.ReactorCore.gameObject );
                break;
            case StructureType.Pillar:
                structureObject = Instantiate( StructurePrefabs.Pillars.Random().gameObject );
                break;
            case StructureType.ResourcesSpot:
                structureObject = Instantiate( StructurePrefabs.ResourceSpots.Random().gameObject );
                break;
            case StructureType.Engine:
                structureObject = Instantiate( StructurePrefabs.Engine.gameObject );
                break;
            case StructureType.InfiniteResourceMetal:
                structureObject = Instantiate( StructurePrefabs.InfiniteMetal.gameObject );
                break;
            case StructureType.InfiniteResourceElectronics:
                structureObject = Instantiate( StructurePrefabs.InfiniteElectronics.gameObject );
                break;
            case StructureType.InfiniteResourceConcrete:
                structureObject = Instantiate( StructurePrefabs.InfiniteConcrete.gameObject );
                break;
        }

        if ( structureObject )
        {
            structureObject.transform.position = GameController.Instance.TileToWorldCentered( position );

            if( rotation >= 0.0f )
            {
                structureObject.transform.rotation = Quaternion.Euler( 0.0f, rotation, 0.0f );
            }
            else 
            {
                structureObject.transform.rotation = Quaternion.Euler( 0.0f, (float)UnityEngine.Random.Range( 0, 4 ) * 90.0f, 0.0f );
            }
            Structure structure = structureObject.GetComponent<Structure>();
            structure.InitializeStructure( position );
            structures[position] = structure;

            if ( tileEvents[position] != null )
                tileEvents[position]( position );

            return structure;
        }

        return null;
    }

    public bool IsInside( Vector2i tile )
    {
        return structures.IsInside( tile );
    }
}
