﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct BoundsI
{
    public Vector2i Min { get; set; }

    public Vector2i Span { get; set; }

    public Vector2i Max { get { return Min + Span; } }
    
    public BoundsI( Vector2i origin, Vector2i span )
    {
        Min = origin;
        Span = span;
    }

    /// <summary>
    /// Simulates rotation of the bounds in place: min and max wont change.
    /// Returns the expected origin position of the rotated bounds.
    /// </summary>
    public Vector2i RotateInPlace( Vector2iRotation rotation )
    {
        Vector2i origin = Vector2i.zero;

        switch( rotation.N )
        {
            case 1:
                origin.y += Span.x - 1;
                break;
            case 2:
                origin.y += Span.y - 1;
                origin.x += Span.x - 1; 
                break;
            case 3:
                origin.x += Span.y - 1;
                break;
        }

        return Min + origin;
    }

    public bool IsInside( Vector2i point )
    {
        return point.x >= Min.x && point.y >= Max.y && point.x < Max.x && point.y < Max.y;
    }
}
