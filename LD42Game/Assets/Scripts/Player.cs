using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Player : MonoBehaviour
{
    public float HitCooldown = .5f;
    [SerializeField]
    private float HitCooldownTimer = 0f;
    private int placing = -1;

    public int Placing { get { return placing; } }

    private float hitLayerTimer;

    public Transform Head;

    public GameObject HitFX;
    public GameObject HitFXSuccess;

    public Interactable interactable;
    public GameObject BuildHoverBox;
    public Color BuildValidColor;
    public Color BuildInvalidColor;

    public static Player Instance { get; private set; }
    
    private void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {

        EvaluateInteraction();

        UpdateHitCycle();

        for( int i = 0; i < 9; ++i )
        {
            if( Input.GetKeyDown( KeyCode.Alpha1 + i ) )
            {
                if( placing == i )
                    placing = -1;
                else
                    placing = i;
            }
        }

        if( Input.GetKeyDown( KeyCode.Escape ) )
        {
            placing = -1;
        }

        if( placing >= 0 )
        {

            Transform cam = Camera.main.transform;
            RaycastHit hit;
            int layer = ~( 1 << LayerMask.NameToLayer( "Ignore Raycast" ) );
            if( Physics.Raycast( new Ray( cam.transform.position, cam.transform.forward ), out hit, 32.0f, layer ) )
            {
                GameController controller = GameController.Instance;
                Vector2i tile = controller.WorldToTile( hit.point );
                Vector3 tileWorldCenter = controller.TileToWorldCentered( tile );

                Vector2i playerTile = controller.WorldToTile( transform.position );

                bool isValid = tile != playerTile && controller.LevelData.IsEmpty( tile );

                if( placing == 3 )
                {
                    isValid = false;
                    if( !controller.LevelData.IsEmpty( tile ) )
                    {
                        Structure structure = controller.LevelData.GetStructure( tile );
                        if( structure.GetComponent<Slum>() )
                        {
                            isValid = true;
                        }
                        else
                        {
                            LivingSpace ls = structure.GetComponent<LivingSpace>();
                            if( ls && ls.NumLayers < 4 && ls.GetComponent<UnCompletedBuilding>().enabled == false )
                            {
                                isValid = true;
                            }
                        }
                    }
                }

                BuildHoverBox.SetActive( true );
                BuildHoverBox.transform.position = tileWorldCenter;
                BuildHoverBox.GetComponent<MeshRenderer>().material.color = isValid ? BuildValidColor : BuildInvalidColor;

                if( Input.GetMouseButtonDown( 0 ) && isValid )
                {
                    bool canBuild = true;
                    List<Resource> costs = GameController.Instance.LevelData.GetStructureCost( IntToStructureType( placing ) );
                    if( costs != null )
                    {
                        foreach( Resource r in costs )
                        {
                            if( PlayerResources.Instance.getStack( r.type ).Amount < r.Amount )
                            {
                                canBuild = false;
                            }
                        }
                    }
                    if( canBuild )
                    {
                        foreach( Resource r in costs )
                        {
                            PlayerResources.Instance.TakeFromStack( r.type, r.Amount );
                        }

                        Structure playerPlaced = controller.AttemptPlace( IntToStructureType( placing ), tile );
                        if( playerPlaced )
                        {
                            SoundBank.Instance.Play( SoundBank.Instance.BuildingPlaced, false );
                            playerPlaced.gameObject.AddComponent<UnCompletedBuilding>();
                        }
                        placing = -1;
                    }
                    else
                    {
                        SoundBank.Instance.Play( SoundBank.Instance.CantPlaceBuilding, true );
                    }
                }
            }
            else
            {
                BuildHoverBox.SetActive( false );
            }
        }
        else
        {
            BuildHoverBox.SetActive( false );
        }

    }

    public StructureType IntToStructureType( int placing )
    {
        switch( placing )
        {
            default:
            case 0:
                return StructureType.AirFilter;
            case 1:
                return StructureType.WaterFilter;
            case 2:
                return StructureType.Farm;
            case 3:
                return StructureType.LivingSpace;
        }


    }


    public void UpdateHitCycle()
    {
        float hitLayerWeight = hitLayerTimer > 0.5f ? 1.0f : ( hitLayerTimer / 0.5f );
        if( HitCooldownTimer > 0f )
        {
            HitCooldownTimer -= Time.deltaTime;
        }

        hitLayerTimer = Mathf.Max( 0.0f, hitLayerTimer - Time.deltaTime );

        GetComponent<Animator>().SetLayerWeight( GetComponent<Animator>().GetLayerIndex( "Shooting" ), hitLayerWeight );

        GetComponent<Animator>().SetFloat( "HitSpeed", 1f - ( HitCooldownTimer / HitCooldown ) );
    }

    public void EvaluateInteraction()
    {
        if( InteractionController.Instance != null )
        {
            interactable = InteractionController.Instance.Interactable;
        }

        bool hitting = Input.GetMouseButton( 0 );

        if( HitCooldownTimer <= 0f && hitting )
        {
            HitCooldownTimer = HitCooldown;
            hitLayerTimer = 1.0f;
            GetComponent<Animator>().SetTrigger( "Hitting" );

            if( interactable != null )
            {
                if( interactable.type != Interactable.InteractionType.Pickable )
                {
                    TryInteract( interactable, InteractionController.Instance.lastInteractableHit.point );
                }
            }
            else
            {
                if( InteractionController.Instance.lastInteractableHit.transform != null )
                {
                    SpawnHitFX( InteractionController.Instance.lastInteractableHit.point, false, .2f );
                }
            }
        }
        if( interactable != null )
        {
            if( interactable.type == Interactable.InteractionType.Pickable )
            {
                if( Input.GetKeyDown( KeyCode.F ) )
                {
                    interactable.Interact();
                }
            }
        }
    }


    public bool TryInteract( Interactable t, Vector3 hitPos )
    {
        if( t == null )
        {
            return false;
        }

        SpawnHitFX( hitPos, true, .2f );

        t.Interact();
        return true;
    }

    private void SpawnHitFX( Vector3 pos, bool success, float delay )
    {
        StartCoroutine( InvokeDelay( () => SpawnHitFX( pos, success ), delay ) );

    }

    private void SpawnHitFX( Vector3 pos, bool success )
    {
        if( HitFX != null )
        {
            GameObject spawn = HitFX;
            if( success )
            {
                spawn = HitFXSuccess;
            }
            Vector3 playerVector = ( pos - Head.position );
            GameObject hitFX = Instantiate( spawn, pos - ( playerVector.normalized * .2f ), Quaternion.LookRotation( playerVector ) );
        }
    }

    private IEnumerator InvokeDelay( System.Action t, float time )
    {
        float timer = 0f;
        while( timer < time )
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        t();
        yield return null;
    }
}
