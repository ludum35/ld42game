using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBank : Singleton<SoundBank>
{

    public AudioClip BuildingPlaced;
    public AudioClip CantPlaceBuilding;
    public AudioSource source;

    public override void Start()
    {
        base.Start();
        source = GetComponent<AudioSource>();
    }

    public void Play( AudioClip p, bool checkIfPlayingSameClip )
    {
        
        if ( checkIfPlayingSameClip )
        {
            if ( source.isPlaying && source.clip == p )
            {
                return;
            }
        }
        source.clip = p;
        source.PlayOneShot( p );
    }
}
