using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

[DefaultExecutionOrder( 200 )]
public class UIBuildingPanels : MonoBehaviour
{
    [Serializable]
    public class UIBuildingPanel
    {
        public StructureType type;
        public TextMeshProUGUI textObj;
        public Transform panel;
    }

    public List<UIBuildingPanel> buildPanels;
    public GameObject panelMask;
    // Use this for initialization
    void Start()
    {

        SetTextToBuildingCost( getPanel(StructureType.AirFilter), GameController.Instance.LevelData.GetStructureCost( StructureType.AirFilter ) );
        SetTextToBuildingCost( getPanel( StructureType.WaterFilter ), GameController.Instance.LevelData.GetStructureCost( StructureType.WaterFilter ) );
        SetTextToBuildingCost( getPanel( StructureType.Farm ), GameController.Instance.LevelData.GetStructureCost( StructureType.Farm ) );
        SetTextToBuildingCost( getPanel( StructureType.LivingSpace ), GameController.Instance.LevelData.GetStructureCost( StructureType.LivingSpace ) );
    }

    public UIBuildingPanel getPanel( StructureType type )
    {
        return buildPanels.Find( ( x ) => x.type == type );
    }

    public void SetTextToBuildingCost( UIBuildingPanel panel, List<Resource> costs )
    {
        string costString = "";
        for ( int i = 0; i < costs.Count; i++ )
        {
            Resource r = costs[i];
            costString += string.Format( "{0:N1} {1}", r.Amount, System.Enum.GetName( typeof( Resources ), r.type ) );
            if ( i != costs.Count - 1 )
            {
                costString += '\n';
            }
        }
        panel.textObj.text = costString;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
