﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// We have to declare serializable classes as non-generic for unity serialization to work.
// Declare them here.
// 
[System.Serializable]
public class SideArrayInt : SideArray<int>
{
    public SideArrayInt() : base() { }
}





[System.Serializable]
public class SideArray<T> : IEnumerable
{
    public T North;
    public T East;
    public T South;
    public T West;

    public T this[Side index]
    {
        get
        {
            switch( index )
            {
                default:
                case Side.North:
                    return North;
                case Side.East:
                    return East;
                case Side.South:
                    return South;
                case Side.West:
                    return West;
            }
        }

        set
        {
            switch( index )
            {
                default:
                case Side.North:
                    North = value;
                    break;
                case Side.East:
                    East = value;
                    break;
                case Side.South:
                    South = value;
                    break;
                case Side.West:
                    West = value;
                    break;
            }
        }
    }

    public IEnumerator GetEnumerator()
    {
        yield return North;
        yield return East; 
        yield return South;
        yield return West;
    }
}
