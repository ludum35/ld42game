using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent( typeof( NavMeshAgent ) )]
[DefaultExecutionOrder( 10 )]
public class NPCAI : MonoBehaviour
{
    public float IdleMinWait = 1.0f;
    public float IdleWaitRange = 9.0f;
    public float AttackDamage = 1.0f;
    public GameObject MolotovPrefab;
    public AudioClip RiotSoundFX;
    public Transform Hand;

    private StoppableCoroutine currentRoutine;
    private NavMeshAgent agent;
    private Animator animator;
    private int speedID;
    private float maxSpeed;
    private bool stop;


    public bool Riotting { get; set; }

    public bool ArrivedAtStructure { get; set; }

    void Start()
    {
        ArrivedAtStructure = true;

        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();

        speedID = Animator.StringToHash( "movementSpeed" );
        maxSpeed = agent.speed;

        // Warp in position so it attaches to the nearest point on navmesh
        agent.Warp( transform.position );
        if ( !agent.isOnNavMesh )
        {
            NavMeshHit hit;
            agent.FindClosestEdge( out hit );
            transform.position = hit.position;
        }

        if ( currentRoutine == null )
            currentRoutine = this.StartCoroutineEx( AIDefaultRoutine() );


        GameController.Instance.NumNPCs++;
    }

    private void OnDestroy()
    {
        stop = true;
        GameController.Instance.NumNPCs--;
    }

    public void MoveToNewSlum( Structure newSlum )
    {
        if ( Riotting )
            return;

        GameController controller = GameController.Instance;

        newSlum.AddNPCAsChild( this );

        if ( currentRoutine != null )
            currentRoutine.Stop();

        currentRoutine = this.StartCoroutineEx( MoveToNewSlumRoutine( newSlum ) );
    }

    public void ReParent( Structure newStructure )
    {
        if ( currentRoutine != null )
            currentRoutine.Stop();

        currentRoutine = this.StartCoroutineEx( ReParentRoutine( newStructure ) );
    }

    public void Riot( Vector2i attackTile )
    {
        Riotting = true;
        if ( currentRoutine != null )
            currentRoutine.Stop();

        currentRoutine = this.StartCoroutineEx( AttackBuildingRoutine( attackTile ) );
    }

    public void FixedUpdate()
    {
        animator.SetFloat( speedID, agent.velocity.magnitude );

        if( currentRoutine == null )
            StartCoroutine( AIDefaultRoutine() );
    }

    public Vector3 GetRandomLocalBorderPosition()
    {
        GameController controller = GameController.Instance;

        int axis = 0;
        float sign = Random.value > 0.5f ? 1.0f : -1.0f;
        if ( Random.value > 0.5f )
        {
            axis = 2;
        }
        int otherAxis = axis == 0 ? 2 : 0;

        Vector3 halfCellSize = controller.CellSize / 2.0f;

        Vector3 localOffset = Vector3.zero;
        localOffset[axis] = ( halfCellSize[axis] - controller.BorderSize + Random.value * controller.BorderSize * 2.0f ) * sign;
        localOffset[otherAxis] = -halfCellSize[otherAxis] + Random.value * controller.CellSize[otherAxis];

        return localOffset;
    }

    private IEnumerator ReParentRoutine( Structure newParent )
    {
        yield return new WaitForSeconds( 0.25f );

        newParent.AddNPCAsChild( this );

        StoppableCoroutine c = this.StartCoroutineEx( AIDefaultRoutine() );
        yield return c.WaitFor();
    }

    private IEnumerator AttackBuildingRoutine( Vector2i targetTile )
    {
        GameController controller = GameController.Instance;

        float tStart = Time.time;
        Structure target = controller.LevelData.GetStructure( targetTile );
        Vector3 moveTo = controller.TileToWorldCentered( targetTile ) + GetRandomLocalBorderPosition();

        StoppableCoroutine move = this.StartCoroutineEx( MoveToPoint( moveTo, 0.0f, false, 60.0f ) );
        yield return move.WaitFor();

        Vector3 d = ( controller.TileToWorldCentered( targetTile ) - transform.position ).normalized;
        transform.rotation = Quaternion.Euler( 0.0f, ( Mathf.Atan2( d.y, d.x ) * Mathf.Rad2Deg ) + 90.0f, 0.0f );

        float actionT = 0.0f;
        float timeLeft = 60.0f - ( Time.time - tStart );
        while ( timeLeft > 0.0f )
        {
            timeLeft -= Time.deltaTime;

            if ( controller.LevelData.IsEmpty( targetTile ) )
            {
                break;
            }

            target.Health -= AttackDamage * Time.deltaTime;

            if ( actionT <= 0.0f )
            {
                actionT = Random.Range( 3.0f, 10.0f );
                int actionID = Random.Range( 0, 4 );

                PlayRiotSound();
                if ( actionID == 0 )
                {
                    animator.SetTrigger( "Attack" );
                    yield return new WaitForSeconds( 0.13f );
                    GameObject molotov = Instantiate( MolotovPrefab );
                    molotov.transform.position = Hand.transform.position;
                    Rigidbody body = molotov.GetComponent<Rigidbody>();
                    body.velocity = new Vector3( d.x, 0.8f, d.z ) * 10.0f;
                    body.angularVelocity = new Vector3( Random.Range( -5.0f, 5.0f ), Random.Range( -5.0f, 5.0f ), Random.Range( -5.0f, 5.0f ) );
                }
                else
                {
                    animator.SetTrigger( "Angry" + actionID );
                }
            }

            actionT -= Time.deltaTime;

            yield return new WaitForFixedUpdate();
        }

        Riotting = false;
        // Return home
        yield return this.StartCoroutineEx( AIDefaultRoutine() ).WaitFor();
    }

    private void PlayRiotSound()
    {
        if ( GetComponent<AudioSource>().isPlaying )
        {
            return;
        }
        GetComponent<AudioSource>().clip = RiotSoundFX;
        GetComponent<AudioSource>().time = Random.Range( 0f, 2f );
        GetComponent<AudioSource>().pitch = 1f + Random.Range( -.2f, .1f );
        GetComponent<AudioSource>().PlayDelayed( Random.Range( 0, 1f ) );
    }

    private IEnumerator MoveToNewSlumRoutine( Structure targetSlum )
    {
        ArrivedAtStructure = false;

        Vector3 target = targetSlum.transform.position + GetRandomLocalBorderPosition();
        yield return this.StartCoroutineEx( MoveToPoint( target, 0.0f, false, 10.0f ) ).WaitFor();

        ArrivedAtStructure = true;

        yield return this.StartCoroutineEx( AIDefaultRoutine() ).WaitFor();
    }

    private IEnumerator AIDefaultRoutine()
    {
        if ( Player.Instance != null )
        {
            // Dont do idle stuff it the NPC is too far away from the player.
            if ( Vector3.Distance( Player.Instance.transform.position, transform.position ) > 100.0f )
            {
                yield return new WaitForSeconds( 5.0f );
            }
        }
        
        float waitTime = IdleMinWait + Random.value * IdleWaitRange;

        yield return new WaitForSeconds( waitTime );

        if ( transform.parent != null )
        {
            Vector3 target = transform.parent.position + GetRandomLocalBorderPosition();

            yield return this.StartCoroutineEx( MoveToPoint( target, 0.5f, true ) ).WaitFor();
        }
    }

    private IEnumerator MoveToPoint( Vector3 point, float waitIfFailed, bool walk, float timeout = -1.0f )
    {

        float t = 0.0f;
        NavMeshPath path = new NavMeshPath();
        if ( agent.CalculatePath( point, path ) )
        {
            agent.speed = walk ? maxSpeed / 3.0f : maxSpeed;
            agent.SetPath( path );

            while ( agent.remainingDistance > 0.1f )
            {
                yield return new WaitForFixedUpdate();

                t += Time.deltaTime;

                if ( timeout > 0.0f && t > timeout )
                    break;
            }
        }
        else if ( waitIfFailed > 0.0f )
        {
            yield return new WaitForSeconds( waitIfFailed );
        }
    }
}
