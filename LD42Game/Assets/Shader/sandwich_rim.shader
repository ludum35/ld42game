// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "sandwich_rim" {//The Shaders Name
//The inputs shown in the material panel
Properties {
	_Albedo_GLTexture ("Albedo - Texture", 2D) = "white" {}
	_Normal_Map_GLTexture ("Normal Map - Texture", 2D) = "white" {}
	_Smoothness ("Smoothness", Range(0.000000000,0.986000000)) = 0.879592600
	_Light_Size ("Light Size", Range(0.000000000,1.000000000)) = 0.595146800
	_Metalness_2_GLTexture ("Metalness 2 - Texture", 2D) = "white" {}
	_Fade_Distance ("Fade Distance", Range(0.000000000,1.000000000)) = 1.000000000
	_Transparency_GLFill ("Transparency - Fill", Range(0.000000000,1.000000000)) = 0.407777800
	_Metalness_GLNumber ("Metalness - Number", Range(0.000000000,1.000000000)) = 0.000000000
	_Emission_GLColor_A ("Emission - Color A", Color) = (1,1,1,1)
	_Albedo_2_Copy_GLMix_Amount ("Albedo 2 Copy - Mix Amount", Range(0.000000000,1.000000000)) = 1.000000000
	_Emission_GLPower ("Emission - Power", Float) = 2.000000000
	_Emission_GLMultiply ("Emission - Multiply", Float) = 0.600000000
	_Normal_Map_GLHeight ("Normal Map - Height", Range(0.000000000,3.000000000)) = 3.000000000
	_Emission_GLMultiply_2 ("Emission - Multiply 2", Float) = 3.800000000
	_Metalness_2_GLPower ("Metalness 2 - Power", Float) = 4.770000000
	SSTEMPSV12816594 ("Texture", 2D) = "white" {}
}

SubShader {
	Tags { "RenderType"="Opaque" "Queue"="AlphaTest" }//A bunch of settings telling Unity a bit about the shader.
	LOD 200
AlphaToMask Off
	Pass {
		Name "FORWARD"
		Tags { "LightMode" = "ForwardBase" }
	ZTest LEqual
	ZWrite On
	Blend Off//No transparency
	Cull Back//Culling specifies which sides of the models faces to hide.

		
		CGPROGRAM
			// compile directives
				#pragma vertex Vertex
				#pragma fragment Pixel
				#pragma target 2.5
				#pragma multi_compile_fog
				#pragma multi_compile __ UNITY_COLORSPACE_GAMMA
				#pragma multi_compile_fwdbase
				#include "HLSLSupport.cginc"
				#include "UnityShaderVariables.cginc"
				#define UNITY_PASS_FORWARDBASE
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "UnityPBSLighting.cginc"
				#include "AutoLight.cginc"

				#define INTERNAL_DATA
				#define WorldReflectionVector(data,normal) data.worldRefl
				#define WorldNormalVector(data,normal) normal
			//Make our inputs accessible by declaring them here.
				sampler2D _Albedo_GLTexture;
float4 _Albedo_GLTexture_ST;
float4 _Albedo_GLTexture_HDR;
				sampler2D _Normal_Map_GLTexture;
float4 _Normal_Map_GLTexture_ST;
float4 _Normal_Map_GLTexture_HDR;
				float _Smoothness;
				float _Light_Size;
				sampler2D _Metalness_2_GLTexture;
float4 _Metalness_2_GLTexture_ST;
float4 _Metalness_2_GLTexture_HDR;
				float _Fade_Distance;
				float _Transparency_GLFill;
				float _Metalness_GLNumber;
				float4 _Emission_GLColor_A;
				float _Albedo_2_Copy_GLMix_Amount;
				float _Emission_GLPower;
				float _Emission_GLMultiply;
				float _Normal_Map_GLHeight;
				float _Emission_GLMultiply_2;
				float _Metalness_2_GLPower;
				sampler2D SSTEMPSV12816594;//Texture
				float4 SSTEMPSV12816594_HDR;//Texture

#if !defined (SSUNITY_BRDF_PBS) // allow to explicitly override BRDF in custom shader
	// still add safe net for low shader models, otherwise we might end up with shaders failing to compile
	#if SHADER_TARGET < 30
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF3)
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF2)
		#define SSUNITY_BRDF_PBS 2
	#elif defined(UNITY_PBS_USE_BRDF1)
		#define SSUNITY_BRDF_PBS 1
	#elif defined(SHADER_TARGET_SURFACE_ANALYSIS)
		// we do preprocess pass during shader analysis and we dont actually care about brdf as we need only inputs/outputs
		#define SSUNITY_BRDF_PBS 1
	#else
		#error something broke in auto-choosing BRDF (Shader Sandwich)
	#endif
#endif

inline half GGXTermNoFadeout (half NdotH, half realRoughness){
	half a2 = realRoughness * realRoughness;
	half d = (NdotH * a2 - NdotH) * NdotH + 1.0f; // 2 mad
	return a2 / (UNITY_PI * d * d);
}

	//From UnityCG.inc, Unity 2017.01 - works better than any of the earlier ones
	inline float3 UnityObjectToWorldNormalNew( in float3 norm ){
		#ifdef UNITY_ASSUME_UNIFORM_SCALING
			return UnityObjectToWorldDir(norm);
		#else
			// mul(IT_M, norm) => mul(norm, I_M) => {dot(norm, I_M.col0), dot(norm, I_M.col1), dot(norm, I_M.col2)}
			return normalize(mul(norm, (float3x3)unity_WorldToObject));
		#endif
	}
	float4 GammaToLinear(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 GammaToLinearForce(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 LinearToGamma(float4 col){
		return col;
	}

	float4 LinearToGammaForWeirdSituations(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinear(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinearForce(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 LinearToGamma(float3 col){
		return col;
	}

	float GammaToLinear(float col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}



















	float RandomNoise3D(float3 co){
		return frac(sin( dot(co.xyz ,float3(12.9898,78.233,45.5432) )) * 43758.5453);
	}
	float RandomNoise2D(float2 co){
		return frac(sin( dot(co.xy ,float2(12.9898,78.233) )) * 43758.5453);
	}
 





struct VertexShaderInput{
	float4 vertex : POSITION;
	float4 tangent : TANGENT;
	float3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
	float4 texcoord1 : TEXCOORD1;
	float4 texcoord2 : TEXCOORD2;
};
struct VertexToPixel{
	float4 position : POSITION;
	float3 screenPos : TEXCOORD0;
	float3 worldViewDir : TEXCOORD1;
	float4 TtoWSpaceX : TEXCOORD2;
	float4 TtoWSpaceY : TEXCOORD3;
	float4 TtoWSpaceZ : TEXCOORD4;
	float2 genericTexcoord : TEXCOORD5;
	float2 uv_Albedo_GLTexture : TEXCOORD6;
	float2 uv_Normal_Map_GLTexture : TEXCOORD7;
	float2 uv_Metalness_2_GLTexture : TEXCOORD8;
	#if UNITY_SHOULD_SAMPLE_SH
		float3 sh : TEXCOORD9;
#endif
	#define pos position
		SHADOW_COORDS(10)
		UNITY_FOG_COORDS(11)
#undef pos
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
		float4 lmap : TEXCOORD12;
	#endif
};

struct VertexData{
	float3 worldPos;
	float4 position;
	float3 worldNormal;
	float3 worldTangent;
	float3 worldBitangent;
	float3 screenPos;
	float3 worldViewDir;
	float3 worldRefl;
	float4 TtoWSpaceX;
	float4 TtoWSpaceY;
	float4 TtoWSpaceZ;
	float2 genericTexcoord;
	float2 uv_Albedo_GLTexture;
	float2 uv_Normal_Map_GLTexture;
	float2 uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
		float3 sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
		float4 lmap;
	#endif
	float Atten;
};
//OutputPremultiplied: False
//UseAlphaGenerate: False
half3 Tangent_Normals ( VertexData vd){
	half3 Surface = half3(0,0,1);
		//Generate layers for the Normals channel.
			//Generate Layer: Normal Map
				//Sample parts of the layer:
					half4 Normal_MapNormals_Sample2 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0.001666666, 0)));
					half4 Normal_MapNormals_Sample3 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0, 0.001666666)));
					half4 Normal_MapNormals_Sample1 = tex2D(_Normal_Map_GLTexture,vd.uv_Normal_Map_GLTexture);
	
				//Apply Effects:
					Normal_MapNormals_Sample1.rgb = normalize(float3(((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample2.a)*_Normal_Map_GLHeight),((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample3.a)*_Normal_Map_GLHeight),1));
	
	Surface = Normal_MapNormals_Sample1.rgb;//0
	
	
	return normalize(half3(dot(vd.TtoWSpaceX.xyz, Surface),dot(vd.TtoWSpaceY.xyz, Surface),dot(vd.TtoWSpaceZ.xyz, Surface)));
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Diffuse ( VertexData vd, UnityGI gi, UnityGIInput giInput, inout half4 previousBaseColor){
	half4 Surface = half4(0,0,0,0);
		//Generate layers for the Albedo channel.
			//Generate Layer: Albedo 2
				//Sample parts of the layer:
					half4 Albedo_2Albedo_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = half4(Albedo_2Albedo_Sample1.rgb,1);//7
	
	
	previousBaseColor = Surface;
	float3 Lighting;
	//Spherical lights implementation modified from http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf - thanks guys! :D
	float3 L = UnityWorldSpaceLightDir(vd.worldPos);
	float3 closestPoint = L + vd.worldNormal * _Light_Size;
	gi.light.dir = normalize(closestPoint);
	#if SSUNITY_BRDF_PBS==1
	half roughness = 1-_Smoothness;
	half3 halfDir = normalize (gi.light.dir + vd.worldViewDir);
	
	half nh = saturate(dot(vd.worldTangent, halfDir));
	half nl = saturate(dot(vd.worldTangent, gi.light.dir));
	half nv = abs(dot(vd.worldNormal, vd.worldViewDir));
	half lh = saturate(dot(gi.light.dir, halfDir));
	
	half nlPow5 = Pow5 (1-nl);
	half nvPow5 = Pow5 (1-nv);
	#else
	Lighting = saturate(dot(vd.worldTangent, gi.light.dir));
	#endif
	Unity_GlossyEnvironmentData g;
	g.roughness = (1-_Smoothness);
	g.reflUVW = vd.worldViewDir;
	gi = UnityGlobalIllumination(giInput, 1, vd.worldTangent,g);
	#if SSUNITY_BRDF_PBS==1
	half Fd90 = 0.5 + 2 * lh * lh * roughness;
	half disneyDiffuse = (1 + (Fd90-1) * nlPow5) * (1 + (Fd90-1) * nvPow5);
	
	half diffuseTerm = disneyDiffuse * nl;
	Lighting = diffuseTerm;
	#endif
	Lighting *= gi.light.color;
	return half4(gi.indirect.diffuse + Lighting,1)*Surface;
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Unlit ( VertexData vd){
	half4 Surface = half4(0.8,0.8,0.8,1.0);
		//Generate layers for the Unlit channel.
			//Generate Layer: Unlit2
				//Sample parts of the layer:
					half4 Unlit2Surface_Sample1 = GammaToLinear(float4(0, 0, 0, 1));
	
	Surface = half4(Unlit2Surface_Sample1.rgb,1) ;//0
	
	
			//Generate Layer: Albedo 2 Copy
				//Sample parts of the layer:
					half4 Albedo_2_CopySurface_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = lerp(Surface,half4(Albedo_2_CopySurface_Sample1.rgb, 1),_Albedo_2_Copy_GLMix_Amount);//2
	
	
	return Surface;
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Specular ( VertexData vd, UnityGI gi, UnityGIInput giInput, inout half4 previousBaseColor, out half OneMinusAlpha){
	half Metalness = 0;//Metallic Mode
		//Generate layers for the Metalness channel.
			//Generate Layer: Metalness 2
				//Sample parts of the layer:
					half4 Metalness_2Specular_Sample1 = tex2D(_Metalness_2_GLTexture,vd.uv_Metalness_2_GLTexture);
	
				//Apply Effects:
					Metalness_2Specular_Sample1.rgb = pow(Metalness_2Specular_Sample1.rgb,_Metalness_2_GLPower);
	
	Metalness = lerp(Metalness,(Metalness + Metalness_2Specular_Sample1.a),Metalness_2Specular_Sample1.a);//1
	
	
			//Generate Layer: Metalness
				//Sample parts of the layer:
					half4 MetalnessSpecular_Sample1 = _Metalness_GLNumber;
	
	Metalness = (Metalness - MetalnessSpecular_Sample1.a);//2
	
	
	half oneMinusRoughness = _Smoothness;
	half perceptualRoughness = 1-oneMinusRoughness;
	half realRoughness = perceptualRoughness*perceptualRoughness;		// need to square perceptual roughness
	half oneMinusReflectivity = OneMinusReflectivityFromMetallic(Metalness);
	half reflectivity = 1-oneMinusReflectivity;
	Unity_GlossyEnvironmentData g;
	g.roughness = (1-_Smoothness);
	g.reflUVW = reflect(-vd.worldViewDir, vd.worldTangent);
	gi = UnityGlobalIllumination(giInput, 1, vd.worldTangent,g);
	//Spherical lights implementation from http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf - thanks guys! :D
	float3 L = UnityWorldSpaceLightDir(vd.worldPos);
	float3 centerToRay = dot(L,vd.worldRefl)*vd.worldRefl-L;
	float3 closestPoint = L + centerToRay * saturate(_Light_Size/length(centerToRay));
	gi.light.dir = normalize(closestPoint);
	half3 halfDir = normalize (gi.light.dir + vd.worldViewDir);
	//#if UNITY_BRDF_GGX 
	//	half shiftAmount = dot(vd.worldTangent, vd.worldViewDir);
	//	vd.worldTangent = shiftAmount < 0.0f ? vd.worldTangent + vd.worldViewDir * (-shiftAmount + 1e-5f) : vd.worldTangent;
	//#endif
	half nh = saturate(dot(vd.worldNormal, halfDir));
	half nl = saturate(dot(vd.worldNormal, gi.light.dir));
	half nv = abs(dot(vd.worldNormal, vd.worldViewDir));
	half lh = saturate(dot(gi.light.dir, halfDir));
	
	half3 Lighting;
	#if SSUNITY_BRDF_PBS==1
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	// surfaceReduction = Int D(NdotH) * NdotH * Id(NdotL>0) dH = 1/(realRoughness^2+1)
	half surfaceReduction;
	if (IsGammaSpace()) surfaceReduction = 1.0 - 0.28*realRoughness*perceptualRoughness;		// 1-0.28*x^3 as approximation for (1/(x^4+1))^(1/2.2) on the domain [0;1]
	else surfaceReduction = 1.0 / (realRoughness*realRoughness + 1.0);			// fade in [0.5;1]
	half whyDoIBotherWithStandardSupport = realRoughness;
	#if UNITY_VERSION<550
		whyDoIBotherWithStandardSupport = perceptualRoughness;
	#endif
	#if UNITY_BRDF_GGX
		half V = SmithJointGGXVisibilityTerm (nl, nv, whyDoIBotherWithStandardSupport);
		half D = GGXTermNoFadeout (nh, realRoughness);
	#else
		half V = SmithBeckmannVisibilityTerm (nl, nv, whyDoIBotherWithStandardSupport);
		half D = NDFBlinnPhongNormalizedTerm (nh, RoughnessToSpecPower (perceptualRoughness));
	#endif
	
	// HACK: theoretically we should divide by Pi diffuseTerm and not multiply specularTerm!
	// BUT 1) that will make shader look significantly darker than Legacy ones
	// and 2) on engine side Non-important lights have to be divided by Pi to in cases when they are injected into ambient SH
	// NOTE: multiplication by Pi is part of single constant together with 1/4 now
	#if UNITY_VERSION>=550//Unity changed the value of PI...:(
		half specularTerm = (V * D) * (UNITY_PI); // Torrance-Sparrow model, Fresnel is applied later (for optimization reasons)
	#else
		half specularTerm = (V * D) * (UNITY_PI/4); // Torrance-Sparrow model, Fresnel is applied later (for optimization reasons)
	#endif
	if (IsGammaSpace())
		specularTerm = sqrt(max(1e-4h, specularTerm));
	specularTerm = max(0, specularTerm * nl);
	
	
	Lighting =	specularTerm;
	#elif SSUNITY_BRDF_PBS==2
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	#if UNITY_BRDF_GGX
	
		// GGX Distribution multiplied by combined approximation of Visibility and Fresnel
		// See "Optimizing PBR for Mobile" from Siggraph 2015 moving mobile graphics course
		// https://community.arm.com/events/1155
		half a = realRoughness;
		half a2 = a*a;
	
		half d = nh * nh * (a2 - 1.h) + 1.00001h;
	#ifdef UNITY_COLORSPACE_GAMMA
		// Tighter approximation for Gamma only rendering mode!
		// DVF = sqrt(DVF);
		// DVF = (a * sqrt(.25)) / (max(sqrt(0.1), lh)*sqrt(realRoughness + .5) * d);
		half specularTerm = a / (max(0.32h, lh) * (1.5h + realRoughness) * d);
	#else
		half specularTerm = a2 / (max(0.1h, lh*lh) * (realRoughness + 0.5h) * (d * d) * 4);
	#endif
	
		// on mobiles (where half actually means something) denominator have risk of overflow
		// clamp below was added specifically to "fix" that, but dx compiler (we convert bytecode to metal/gles)
		// sees that specularTerm have only non-negative terms, so it skips max(0,..) in clamp (leaving only min(100,...))
	#if defined (SHADER_API_MOBILE)
		specularTerm = specularTerm - 1e-4h;
	#endif
	
	#else
	
		// Legacy
		half specularPower = RoughnessToSpecPower(perceptualRoughness);
		// Modified with approximate Visibility function that takes roughness into account
		// Original ((n+1)*N.H^n) / (8*Pi * L.H^3) didn't take into account roughness
		// and produced extremely bright specular at grazing angles
	
		half invV = lh * lh * smoothness + perceptualRoughness * perceptualRoughness; // approx ModifiedKelemenVisibilityTerm(lh, perceptualRoughness);
		half invF = lh;
	
		half specularTerm = ((specularPower + 1) * pow (nh, specularPower)) / (8 * invV * invF + 1e-4h);
	
	#ifdef UNITY_COLORSPACE_GAMMA
		specularTerm = sqrt(max(1e-4h, specularTerm));
	#endif
	
	#endif
	
	#if defined (SHADER_API_MOBILE)
		specularTerm = clamp(specularTerm, 0.0, 100.0); // Prevent FP16 overflow on mobiles
	#endif
	#if defined(_SPECULARHIGHLIGHTS_OFF)
		specularTerm = 0.0;
	#endif
	
		// surfaceReduction = Int D(NdotH) * NdotH * Id(NdotL>0) dH = 1/(realRoughness^2+1)
	
		// 1-0.28*x^3 as approximation for (1/(x^4+1))^(1/2.2) on the domain [0;1]
		// 1-x^3*(0.6-0.08*x)   approximation for 1/(x^4+1)
	#ifdef UNITY_COLORSPACE_GAMMA
		half surfaceReduction = 0.28;
	#else
		half surfaceReduction = (0.6-0.08*perceptualRoughness);
	#endif
		surfaceReduction = 1.0 - realRoughness*perceptualRoughness*surfaceReduction;
	
	Lighting =	specularTerm;
	#else
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	half surfaceReduction = 1;
	half2 rlPow4AndFresnelTerm = Pow4 (half2(dot(vd.worldRefl, gi.light.dir), 1-nv));
	half rlPow4 = rlPow4AndFresnelTerm.x; // power exponent must match kHorizontalWarpExp in NHxRoughness() function in GeneratedTextures.cpp
	half fresnelTerm = rlPow4AndFresnelTerm.y;
	
	
	Lighting =	BRDF3_Direct(0,1,rlPow4,oneMinusRoughness)*nl;
	#endif
	half awithalittlething = (realRoughness + ((_Light_Size)/(2*length(L))));
	awithalittlething = realRoughness/awithalittlething;
	Lighting *= max(1e-4f,awithalittlething);
	Lighting *= gi.light.color;
	half grazingTerm = saturate((oneMinusRoughness) + (reflectivity));
	#if SSUNITY_BRDF_PBS==1
	Lighting =	Lighting * FresnelTerm (Surface, lh) + surfaceReduction * gi.indirect.specular * FresnelLerp (Surface.rgb, grazingTerm, nv);
	#elif SSUNITY_BRDF_PBS==2
	Lighting =	(Lighting * nl + surfaceReduction * gi.indirect.specular) * FresnelLerpFast (Surface.rgb, grazingTerm, nv);
	#else
	Lighting = (Lighting+gi.indirect.specular) * lerp (Surface.rgb, grazingTerm, fresnelTerm);
	#endif
	OneMinusAlpha = oneMinusReflectivity;
	return half4(Lighting,reflectivity);
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Emission ( VertexData vd){
	half4 Surface = half4(0,0,0,0);
		//Generate layers for the Emission channel.
			//Generate Layer: Emission2
				//Sample parts of the layer:
					half4 Emission2Emission_Sample1 = GammaToLinear(float4(0, 0, 0, 1));
	
	Surface = half4(Emission2Emission_Sample1.rgb,1) ;//0
	
	
			//Generate Layer: Emission
				//Sample parts of the layer:
					half4 EmissionEmission_Sample1 = tex2D(SSTEMPSV12816594,vd.genericTexcoord);
	
				//Apply Effects:
					EmissionEmission_Sample1.rgb = (EmissionEmission_Sample1.rgb*_Emission_GLMultiply_2);
	
	Surface = half4((Surface.rgb + EmissionEmission_Sample1.rgb),1) ;//0
	
	
			//Generate Layer: Emission 2
				//Sample parts of the layer:
					half4 Emission_2Emission_Sample1 = lerp(GammaToLinear(float4(0, 0, 0, 0)), _Emission_GLColor_A, GammaToLinear((1-dot(vd.worldNormal, vd.worldViewDir))));
	
				//Apply Effects:
					Emission_2Emission_Sample1.rgb = pow(Emission_2Emission_Sample1.rgb,_Emission_GLPower);
					Emission_2Emission_Sample1.rgb = (Emission_2Emission_Sample1.rgb*_Emission_GLMultiply);
	
	Surface = half4((Surface.rgb + Emission_2Emission_Sample1.rgb),1) ;//0
	
	
	return Surface;
}
//OutputPremultiplied: False
//UseAlphaGenerate: True
half4 Set_Alpha_Channel ( VertexData vd, VertexToPixel vtp, half4 outputColor){
	float Surface = 0;
		//Generate layers for the Transparency channel.
			//Generate Layer: Transparency
				//Sample parts of the layer:
					half4 TransparencyTransparency_Sample1 = GammaToLinear(RandomNoise2D(vtp.position.xyz.xy));
	
	Surface = TransparencyTransparency_Sample1.a;//2
	
	
			//Generate Layer: Transparency6
				//Sample parts of the layer:
					half4 Transparency6Transparency_Sample1 = 0.5294118;
	
				//Apply Effects:
					Transparency6Transparency_Sample1.a = 1;
					Transparency6Transparency_Sample1.a = (Transparency6Transparency_Sample1.a*(saturate((vd.screenPos.z-(0))/(1.92-0))));
	
	Surface = lerp(Surface,1,Transparency6Transparency_Sample1.a);//1
	
	
	
	return float4(outputColor.rgb,Surface);
	
}
float4 Transparency ( float4 outputColor){
	outputColor.a -= _Fade_Distance;
	clip(outputColor.a);
	return outputColor;
	
}
VertexToPixel Vertex (VertexShaderInput v){
	VertexToPixel vtp;
	UNITY_INITIALIZE_OUTPUT(VertexToPixel,vtp);
	VertexData vd;
	UNITY_INITIALIZE_OUTPUT(VertexData,vd);
	vd.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	vd.position = UnityObjectToClipPos(v.vertex);
	vd.worldNormal = UnityObjectToWorldNormalNew(v.normal);
	vd.worldTangent = UnityObjectToWorldNormalNew(v.tangent);
	vd.worldBitangent = cross(vd.worldNormal, vd.worldTangent) * v.tangent.w * unity_WorldTransformParams.w;
	vd.screenPos = ComputeScreenPos (UnityObjectToClipPos (v.vertex)).xyw;
	vd.worldViewDir = normalize(UnityWorldSpaceViewDir(vd.worldPos));
	vd.TtoWSpaceX = float4(vd.worldTangent.x, vd.worldBitangent.x, vd.worldNormal.x, vd.worldPos.x);
	vd.TtoWSpaceY = float4(vd.worldTangent.y, vd.worldBitangent.y, vd.worldNormal.y, vd.worldPos.y);
	vd.TtoWSpaceZ = float4(vd.worldTangent.z, vd.worldBitangent.z, vd.worldNormal.z, vd.worldPos.z);
	vd.genericTexcoord = v.texcoord;
	vd.uv_Albedo_GLTexture = TRANSFORM_TEX(v.texcoord, _Albedo_GLTexture);
	vd.uv_Normal_Map_GLTexture = TRANSFORM_TEX(v.texcoord, _Normal_Map_GLTexture);
	vd.uv_Metalness_2_GLTexture = TRANSFORM_TEX(v.texcoord, _Metalness_2_GLTexture);
	#if UNITY_SHOULD_SAMPLE_SH
	vd.sh = 0;
// SH/ambient and vertex lights
#ifdef LIGHTMAP_OFF
	vd.sh = 0;
	// Approximated illumination from non-important point lights
	#ifdef VERTEXLIGHT_ON
		vd.sh += Shade4PointLights (
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, vd.worldPos, vd.worldNormal);
	#endif
	vd.sh = ShadeSHPerVertex (vd.worldNormal, vd.sh);
#endif // LIGHTMAP_OFF
;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
	vd.lmap = 0;
#ifndef DYNAMICLIGHTMAP_OFF
	vd.lmap.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
#endif
#ifndef LIGHTMAP_OFF
	vd.lmap.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
#endif
;
	#endif
	
	
	
	
	vtp.position = vd.position;
	vtp.screenPos = vd.screenPos;
	vtp.worldViewDir = vd.worldViewDir;
	vtp.TtoWSpaceX = vd.TtoWSpaceX;
	vtp.TtoWSpaceY = vd.TtoWSpaceY;
	vtp.TtoWSpaceZ = vd.TtoWSpaceZ;
	vtp.genericTexcoord = vd.genericTexcoord;
	vtp.uv_Albedo_GLTexture = vd.uv_Albedo_GLTexture;
	vtp.uv_Normal_Map_GLTexture = vd.uv_Normal_Map_GLTexture;
	vtp.uv_Metalness_2_GLTexture = vd.uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
	vtp.sh = vd.sh;
#endif
	#define pos position
	TRANSFER_SHADOW(vtp);
	UNITY_TRANSFER_FOG(vtp,vtp.pos);
#undef pos
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
	vtp.lmap = vd.lmap;
	#endif
	return vtp;
}
			half4 Pixel (VertexToPixel vtp) : SV_Target {
				half4 outputColor = half4(0,0,0,0);
				half3 outputNormal = half3(0,0,1);
				half3 depth = half3(0,0,0);//Tangent Corrected depth, World Space depth, Normalized depth
				half3 tempDepth = half3(0,0,0);
				VertexData vd;
				UNITY_INITIALIZE_OUTPUT(VertexData,vd);
				vd.worldPos = half3(vtp.TtoWSpaceX.w,vtp.TtoWSpaceY.w,vtp.TtoWSpaceZ.w);
				vd.worldNormal = half3(vtp.TtoWSpaceX.z,vtp.TtoWSpaceY.z,vtp.TtoWSpaceZ.z);
				vd.worldTangent = half3(vtp.TtoWSpaceX.x,vtp.TtoWSpaceY.x,vtp.TtoWSpaceZ.x);
				vd.screenPos = float3(vtp.screenPos.xy/vtp.screenPos.z,vtp.screenPos.z);
				vd.worldViewDir = normalize(UnityWorldSpaceViewDir(vd.worldPos));
				vd.worldRefl = reflect(-vd.worldViewDir, vd.worldNormal);
				vd.TtoWSpaceX = vtp.TtoWSpaceX;
				vd.TtoWSpaceY = vtp.TtoWSpaceY;
				vd.TtoWSpaceZ = vtp.TtoWSpaceZ;
				vd.genericTexcoord = vtp.genericTexcoord;
				vd.uv_Albedo_GLTexture = vtp.uv_Albedo_GLTexture;
				vd.uv_Normal_Map_GLTexture = vtp.uv_Normal_Map_GLTexture;
				vd.uv_Metalness_2_GLTexture = vtp.uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
				vd.sh = vtp.sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
				vd.lmap = vtp.lmap;
	#endif
				half4 previousBaseColor = 0;//Honestly just a quick hack to get Metal specular working XD
				half OneMinusAlpha = 0; //An optimization to avoid redundant 1-a if already calculated in ingredient function :)
				outputNormal = vd.worldNormal;
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT(UnityGI,gi);
// Setup lighting environment
				#ifndef USING_DIRECTIONAL_LIGHT
					fixed3 lightDir = normalize(UnityWorldSpaceLightDir(vd.worldPos));
				#else
					fixed3 lightDir = _WorldSpaceLightPos0.xyz;
				#endif
				//Uses SHADOW_COORDS
				UNITY_LIGHT_ATTENUATION(atten, vtp, vd.worldPos)
				vd.Atten = atten;
				gi.indirect.diffuse = 0;
				gi.indirect.specular = 0;
				#if !defined(LIGHTMAP_ON)
					gi.light.color = _LightColor0.rgb;
					gi.light.dir = lightDir;
					gi.light.ndotl = LambertTerm (vd.worldNormal, gi.light.dir);
				#endif
				// Call GI (lightmaps/SH/reflections) lighting function
				UnityGIInput giInput;
				UNITY_INITIALIZE_OUTPUT(UnityGIInput, giInput);
				giInput.light = gi.light;
				giInput.worldPos = vd.worldPos;
				giInput.worldViewDir = vd.worldViewDir;
				giInput.atten = atten;
				#if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
					giInput.lightmapUV = vd.lmap;
				#else
					giInput.lightmapUV = 0.0;
				#endif
				#if UNITY_SHOULD_SAMPLE_SH
					giInput.ambient = vd.sh;
				#else
					giInput.ambient.rgb = 0.0;
				#endif
				giInput.probeHDR[0] = unity_SpecCube0_HDR;
				giInput.probeHDR[1] = unity_SpecCube1_HDR;
				#if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
					giInput.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
				#endif
				#if UNITY_SPECCUBE_BOX_PROJECTION
					giInput.boxMax[0] = unity_SpecCube0_BoxMax;
					giInput.probePosition[0] = unity_SpecCube0_ProbePosition;
					giInput.boxMax[1] = unity_SpecCube1_BoxMax;
					giInput.boxMin[1] = unity_SpecCube1_BoxMin;
					giInput.probePosition[1] = unity_SpecCube1_ProbePosition;
				#endif
				half3 outputTangent_Normals = Tangent_Normals ( vd);
				outputNormal = outputTangent_Normals.rgb;//0
								vd.worldNormal = outputNormal;
				vd.worldRefl = reflect(-vd.worldViewDir, vd.worldNormal);
				half4 outputDiffuse = Diffuse ( vd, gi, giInput, previousBaseColor);
				outputColor = half4(outputDiffuse.rgb,1);//7
								half4 outputUnlit = Unlit ( vd);
				outputColor = half4((outputColor.rgb + outputUnlit.rgb),1) ;//0
								half4 outputSpecular = Specular ( vd, gi, giInput, previousBaseColor, OneMinusAlpha);
				outputColor = half4(outputSpecular.rgb,1) ;//0
								half4 outputEmission = Emission ( vd);
				outputColor = half4((outputColor.rgb + outputEmission.rgb),1) ;//0
								half4 outputSet_Alpha_Channel = Set_Alpha_Channel ( vd, vtp, outputColor);
				outputColor = outputSet_Alpha_Channel;//10
								outputColor = Transparency ( outputColor);
				UNITY_APPLY_FOG(vtp.fogCoord, outputColor); // apply fog (UNITY_FOG_COORDS));
				return outputColor;

			}
		ENDCG
	}
AlphaToMask Off
	Pass {
		Name "FORWARD"
		Tags { "LightMode" = "ForwardAdd" }
	Fog { Color (0,0,0,0) }
	ZTest LEqual
	ZWrite On
	Blend One One
	Cull Back//Culling specifies which sides of the models faces to hide.

		
		CGPROGRAM
			// compile directives
				#pragma vertex Vertex
				#pragma fragment Pixel
				#pragma target 2.5
				#pragma multi_compile_fog
				#pragma multi_compile __ UNITY_COLORSPACE_GAMMA
				#pragma multi_compile_fwdadd_fullshadows
				#include "HLSLSupport.cginc"
				#include "UnityShaderVariables.cginc"
				#define UNITY_PASS_FORWARDADD
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "UnityPBSLighting.cginc"
				#include "AutoLight.cginc"

				#define INTERNAL_DATA
				#define WorldReflectionVector(data,normal) data.worldRefl
				#define WorldNormalVector(data,normal) normal
			//Make our inputs accessible by declaring them here.
				sampler2D _Albedo_GLTexture;
float4 _Albedo_GLTexture_ST;
float4 _Albedo_GLTexture_HDR;
				sampler2D _Normal_Map_GLTexture;
float4 _Normal_Map_GLTexture_ST;
float4 _Normal_Map_GLTexture_HDR;
				float _Smoothness;
				float _Light_Size;
				sampler2D _Metalness_2_GLTexture;
float4 _Metalness_2_GLTexture_ST;
float4 _Metalness_2_GLTexture_HDR;
				float _Fade_Distance;
				float _Transparency_GLFill;
				float _Metalness_GLNumber;
				float4 _Emission_GLColor_A;
				float _Albedo_2_Copy_GLMix_Amount;
				float _Emission_GLPower;
				float _Emission_GLMultiply;
				float _Normal_Map_GLHeight;
				float _Emission_GLMultiply_2;
				float _Metalness_2_GLPower;

#if !defined (SSUNITY_BRDF_PBS) // allow to explicitly override BRDF in custom shader
	// still add safe net for low shader models, otherwise we might end up with shaders failing to compile
	#if SHADER_TARGET < 30
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF3)
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF2)
		#define SSUNITY_BRDF_PBS 2
	#elif defined(UNITY_PBS_USE_BRDF1)
		#define SSUNITY_BRDF_PBS 1
	#elif defined(SHADER_TARGET_SURFACE_ANALYSIS)
		// we do preprocess pass during shader analysis and we dont actually care about brdf as we need only inputs/outputs
		#define SSUNITY_BRDF_PBS 1
	#else
		#error something broke in auto-choosing BRDF (Shader Sandwich)
	#endif
#endif

inline half GGXTermNoFadeout (half NdotH, half realRoughness){
	half a2 = realRoughness * realRoughness;
	half d = (NdotH * a2 - NdotH) * NdotH + 1.0f; // 2 mad
	return a2 / (UNITY_PI * d * d);
}

	//From UnityCG.inc, Unity 2017.01 - works better than any of the earlier ones
	inline float3 UnityObjectToWorldNormalNew( in float3 norm ){
		#ifdef UNITY_ASSUME_UNIFORM_SCALING
			return UnityObjectToWorldDir(norm);
		#else
			// mul(IT_M, norm) => mul(norm, I_M) => {dot(norm, I_M.col0), dot(norm, I_M.col1), dot(norm, I_M.col2)}
			return normalize(mul(norm, (float3x3)unity_WorldToObject));
		#endif
	}
	float4 GammaToLinear(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 GammaToLinearForce(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 LinearToGamma(float4 col){
		return col;
	}

	float4 LinearToGammaForWeirdSituations(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinear(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinearForce(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 LinearToGamma(float3 col){
		return col;
	}

	float GammaToLinear(float col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}



















	float RandomNoise3D(float3 co){
		return frac(sin( dot(co.xyz ,float3(12.9898,78.233,45.5432) )) * 43758.5453);
	}
	float RandomNoise2D(float2 co){
		return frac(sin( dot(co.xy ,float2(12.9898,78.233) )) * 43758.5453);
	}
 





struct VertexShaderInput{
	float4 vertex : POSITION;
	float4 tangent : TANGENT;
	float3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
	float4 texcoord1 : TEXCOORD1;
	float4 texcoord2 : TEXCOORD2;
};
struct VertexToPixel{
	float4 position : POSITION;
	float3 screenPos : TEXCOORD0;
	float3 worldViewDir : TEXCOORD1;
	float4 TtoWSpaceX : TEXCOORD2;
	float4 TtoWSpaceY : TEXCOORD3;
	float4 TtoWSpaceZ : TEXCOORD4;
	float2 uv_Albedo_GLTexture : TEXCOORD5;
	float2 uv_Normal_Map_GLTexture : TEXCOORD6;
	float2 uv_Metalness_2_GLTexture : TEXCOORD7;
	#if UNITY_SHOULD_SAMPLE_SH
		float3 sh : TEXCOORD8;
#endif
	#define pos position
		SHADOW_COORDS(9)
		UNITY_FOG_COORDS(10)
#undef pos
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
		float4 lmap : TEXCOORD11;
	#endif
};

struct VertexData{
	float3 worldPos;
	float4 position;
	float3 worldNormal;
	float3 worldTangent;
	float3 worldBitangent;
	float3 screenPos;
	float3 worldViewDir;
	float3 worldRefl;
	float4 TtoWSpaceX;
	float4 TtoWSpaceY;
	float4 TtoWSpaceZ;
	float2 uv_Albedo_GLTexture;
	float2 uv_Normal_Map_GLTexture;
	float2 uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
		float3 sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
		float4 lmap;
	#endif
	float Atten;
};
//OutputPremultiplied: False
//UseAlphaGenerate: False
half3 Tangent_Normals ( VertexData vd){
	half3 Surface = half3(0,0,1);
		//Generate layers for the Normals channel.
			//Generate Layer: Normal Map
				//Sample parts of the layer:
					half4 Normal_MapNormals_Sample2 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0.001666666, 0)));
					half4 Normal_MapNormals_Sample3 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0, 0.001666666)));
					half4 Normal_MapNormals_Sample1 = tex2D(_Normal_Map_GLTexture,vd.uv_Normal_Map_GLTexture);
	
				//Apply Effects:
					Normal_MapNormals_Sample1.rgb = normalize(float3(((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample2.a)*_Normal_Map_GLHeight),((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample3.a)*_Normal_Map_GLHeight),1));
	
	Surface = Normal_MapNormals_Sample1.rgb;//0
	
	
	return normalize(half3(dot(vd.TtoWSpaceX.xyz, Surface),dot(vd.TtoWSpaceY.xyz, Surface),dot(vd.TtoWSpaceZ.xyz, Surface)));
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Diffuse ( VertexData vd, UnityGI gi, UnityGIInput giInput, inout half4 previousBaseColor){
	half4 Surface = half4(0,0,0,0);
		//Generate layers for the Albedo channel.
			//Generate Layer: Albedo 2
				//Sample parts of the layer:
					half4 Albedo_2Albedo_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = half4(Albedo_2Albedo_Sample1.rgb,1);//7
	
	
	previousBaseColor = Surface;
	float3 Lighting;
	//Spherical lights implementation modified from http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf - thanks guys! :D
	float3 L = UnityWorldSpaceLightDir(vd.worldPos);
	float3 closestPoint = L + vd.worldNormal * _Light_Size;
	gi.light.dir = normalize(closestPoint);
	#if SSUNITY_BRDF_PBS==1
	half roughness = 1-_Smoothness;
	half3 halfDir = normalize (gi.light.dir + vd.worldViewDir);
	
	half nh = saturate(dot(vd.worldTangent, halfDir));
	half nl = saturate(dot(vd.worldTangent, gi.light.dir));
	half nv = abs(dot(vd.worldNormal, vd.worldViewDir));
	half lh = saturate(dot(gi.light.dir, halfDir));
	
	half nlPow5 = Pow5 (1-nl);
	half nvPow5 = Pow5 (1-nv);
	#else
	Lighting = saturate(dot(vd.worldTangent, gi.light.dir));
	#endif
	UnityGI o_gi;
	ResetUnityGI(o_gi);
	o_gi.light = giInput.light;
	o_gi.light.color *= giInput.atten;
	gi = o_gi;
	#if SSUNITY_BRDF_PBS==1
	half Fd90 = 0.5 + 2 * lh * lh * roughness;
	half disneyDiffuse = (1 + (Fd90-1) * nlPow5) * (1 + (Fd90-1) * nvPow5);
	
	half diffuseTerm = disneyDiffuse * nl;
	Lighting = diffuseTerm;
	#endif
	Lighting *= gi.light.color;
	return half4(Lighting,1)*Surface;
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Unlit ( VertexData vd){
	half4 Surface = half4(0.8,0.8,0.8,1.0);
		//Generate layers for the Unlit channel.
			//Generate Layer: Unlit2
				//Sample parts of the layer:
					half4 Unlit2Surface_Sample1 = GammaToLinear(float4(0, 0, 0, 1));
	
	Surface = half4(Unlit2Surface_Sample1.rgb,1) ;//0
	
	
			//Generate Layer: Albedo 2 Copy
				//Sample parts of the layer:
					half4 Albedo_2_CopySurface_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = lerp(Surface,half4(Albedo_2_CopySurface_Sample1.rgb, 1),_Albedo_2_Copy_GLMix_Amount);//2
	
	
	Surface.rgb = 0;
	return Surface;
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Specular ( VertexData vd, UnityGI gi, UnityGIInput giInput, inout half4 previousBaseColor, out half OneMinusAlpha){
	half Metalness = 0;//Metallic Mode
		//Generate layers for the Metalness channel.
			//Generate Layer: Metalness 2
				//Sample parts of the layer:
					half4 Metalness_2Specular_Sample1 = tex2D(_Metalness_2_GLTexture,vd.uv_Metalness_2_GLTexture);
	
				//Apply Effects:
					Metalness_2Specular_Sample1.rgb = pow(Metalness_2Specular_Sample1.rgb,_Metalness_2_GLPower);
	
	Metalness = lerp(Metalness,(Metalness + Metalness_2Specular_Sample1.a),Metalness_2Specular_Sample1.a);//1
	
	
			//Generate Layer: Metalness
				//Sample parts of the layer:
					half4 MetalnessSpecular_Sample1 = _Metalness_GLNumber;
	
	Metalness = (Metalness - MetalnessSpecular_Sample1.a);//2
	
	
	half oneMinusRoughness = _Smoothness;
	half perceptualRoughness = 1-oneMinusRoughness;
	half realRoughness = perceptualRoughness*perceptualRoughness;		// need to square perceptual roughness
	half oneMinusReflectivity = OneMinusReflectivityFromMetallic(Metalness);
	half reflectivity = 1-oneMinusReflectivity;
	UnityGI o_gi;
	ResetUnityGI(o_gi);
	o_gi.light = giInput.light;
	o_gi.light.color *= giInput.atten;
	gi = o_gi;
	//Spherical lights implementation from http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf - thanks guys! :D
	float3 L = UnityWorldSpaceLightDir(vd.worldPos);
	float3 centerToRay = dot(L,vd.worldRefl)*vd.worldRefl-L;
	float3 closestPoint = L + centerToRay * saturate(_Light_Size/length(centerToRay));
	gi.light.dir = normalize(closestPoint);
	half3 halfDir = normalize (gi.light.dir + vd.worldViewDir);
	//#if UNITY_BRDF_GGX 
	//	half shiftAmount = dot(vd.worldTangent, vd.worldViewDir);
	//	vd.worldTangent = shiftAmount < 0.0f ? vd.worldTangent + vd.worldViewDir * (-shiftAmount + 1e-5f) : vd.worldTangent;
	//#endif
	half nh = saturate(dot(vd.worldNormal, halfDir));
	half nl = saturate(dot(vd.worldNormal, gi.light.dir));
	half nv = abs(dot(vd.worldNormal, vd.worldViewDir));
	half lh = saturate(dot(gi.light.dir, halfDir));
	
	half3 Lighting;
	#if SSUNITY_BRDF_PBS==1
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	// surfaceReduction = Int D(NdotH) * NdotH * Id(NdotL>0) dH = 1/(realRoughness^2+1)
	half surfaceReduction;
	if (IsGammaSpace()) surfaceReduction = 1.0 - 0.28*realRoughness*perceptualRoughness;		// 1-0.28*x^3 as approximation for (1/(x^4+1))^(1/2.2) on the domain [0;1]
	else surfaceReduction = 1.0 / (realRoughness*realRoughness + 1.0);			// fade in [0.5;1]
	half whyDoIBotherWithStandardSupport = realRoughness;
	#if UNITY_VERSION<550
		whyDoIBotherWithStandardSupport = perceptualRoughness;
	#endif
	#if UNITY_BRDF_GGX
		half V = SmithJointGGXVisibilityTerm (nl, nv, whyDoIBotherWithStandardSupport);
		half D = GGXTermNoFadeout (nh, realRoughness);
	#else
		half V = SmithBeckmannVisibilityTerm (nl, nv, whyDoIBotherWithStandardSupport);
		half D = NDFBlinnPhongNormalizedTerm (nh, RoughnessToSpecPower (perceptualRoughness));
	#endif
	
	// HACK: theoretically we should divide by Pi diffuseTerm and not multiply specularTerm!
	// BUT 1) that will make shader look significantly darker than Legacy ones
	// and 2) on engine side Non-important lights have to be divided by Pi to in cases when they are injected into ambient SH
	// NOTE: multiplication by Pi is part of single constant together with 1/4 now
	#if UNITY_VERSION>=550//Unity changed the value of PI...:(
		half specularTerm = (V * D) * (UNITY_PI); // Torrance-Sparrow model, Fresnel is applied later (for optimization reasons)
	#else
		half specularTerm = (V * D) * (UNITY_PI/4); // Torrance-Sparrow model, Fresnel is applied later (for optimization reasons)
	#endif
	if (IsGammaSpace())
		specularTerm = sqrt(max(1e-4h, specularTerm));
	specularTerm = max(0, specularTerm * nl);
	
	
	Lighting =	specularTerm;
	#elif SSUNITY_BRDF_PBS==2
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	#if UNITY_BRDF_GGX
	
		// GGX Distribution multiplied by combined approximation of Visibility and Fresnel
		// See "Optimizing PBR for Mobile" from Siggraph 2015 moving mobile graphics course
		// https://community.arm.com/events/1155
		half a = realRoughness;
		half a2 = a*a;
	
		half d = nh * nh * (a2 - 1.h) + 1.00001h;
	#ifdef UNITY_COLORSPACE_GAMMA
		// Tighter approximation for Gamma only rendering mode!
		// DVF = sqrt(DVF);
		// DVF = (a * sqrt(.25)) / (max(sqrt(0.1), lh)*sqrt(realRoughness + .5) * d);
		half specularTerm = a / (max(0.32h, lh) * (1.5h + realRoughness) * d);
	#else
		half specularTerm = a2 / (max(0.1h, lh*lh) * (realRoughness + 0.5h) * (d * d) * 4);
	#endif
	
		// on mobiles (where half actually means something) denominator have risk of overflow
		// clamp below was added specifically to "fix" that, but dx compiler (we convert bytecode to metal/gles)
		// sees that specularTerm have only non-negative terms, so it skips max(0,..) in clamp (leaving only min(100,...))
	#if defined (SHADER_API_MOBILE)
		specularTerm = specularTerm - 1e-4h;
	#endif
	
	#else
	
		// Legacy
		half specularPower = RoughnessToSpecPower(perceptualRoughness);
		// Modified with approximate Visibility function that takes roughness into account
		// Original ((n+1)*N.H^n) / (8*Pi * L.H^3) didn't take into account roughness
		// and produced extremely bright specular at grazing angles
	
		half invV = lh * lh * smoothness + perceptualRoughness * perceptualRoughness; // approx ModifiedKelemenVisibilityTerm(lh, perceptualRoughness);
		half invF = lh;
	
		half specularTerm = ((specularPower + 1) * pow (nh, specularPower)) / (8 * invV * invF + 1e-4h);
	
	#ifdef UNITY_COLORSPACE_GAMMA
		specularTerm = sqrt(max(1e-4h, specularTerm));
	#endif
	
	#endif
	
	#if defined (SHADER_API_MOBILE)
		specularTerm = clamp(specularTerm, 0.0, 100.0); // Prevent FP16 overflow on mobiles
	#endif
	#if defined(_SPECULARHIGHLIGHTS_OFF)
		specularTerm = 0.0;
	#endif
	
		// surfaceReduction = Int D(NdotH) * NdotH * Id(NdotL>0) dH = 1/(realRoughness^2+1)
	
		// 1-0.28*x^3 as approximation for (1/(x^4+1))^(1/2.2) on the domain [0;1]
		// 1-x^3*(0.6-0.08*x)   approximation for 1/(x^4+1)
	#ifdef UNITY_COLORSPACE_GAMMA
		half surfaceReduction = 0.28;
	#else
		half surfaceReduction = (0.6-0.08*perceptualRoughness);
	#endif
		surfaceReduction = 1.0 - realRoughness*perceptualRoughness*surfaceReduction;
	
	Lighting =	specularTerm;
	#else
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	half surfaceReduction = 1;
	half2 rlPow4AndFresnelTerm = Pow4 (half2(dot(vd.worldRefl, gi.light.dir), 1-nv));
	half rlPow4 = rlPow4AndFresnelTerm.x; // power exponent must match kHorizontalWarpExp in NHxRoughness() function in GeneratedTextures.cpp
	half fresnelTerm = rlPow4AndFresnelTerm.y;
	
	
	Lighting =	BRDF3_Direct(0,1,rlPow4,oneMinusRoughness)*nl;
	#endif
	half awithalittlething = (realRoughness + ((_Light_Size)/(2*length(L))));
	awithalittlething = realRoughness/awithalittlething;
	Lighting *= max(1e-4f,awithalittlething);
	Lighting *= gi.light.color;
	#if SSUNITY_BRDF_PBS==1
	Lighting =	Lighting * FresnelTerm (Surface, lh);
	#elif SSUNITY_BRDF_PBS==2
	Lighting =	Lighting * nl * Surface;
	#elif SSUNITY_BRDF_PBS==3
	Lighting =	Lighting * Surface;
	#endif
	OneMinusAlpha = oneMinusReflectivity;
	return half4(Lighting,reflectivity);
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: True
half4 Set_Alpha_Channel ( VertexData vd, VertexToPixel vtp, half4 outputColor){
	float Surface = 0;
		//Generate layers for the Transparency channel.
			//Generate Layer: Transparency
				//Sample parts of the layer:
					half4 TransparencyTransparency_Sample1 = GammaToLinear(RandomNoise2D(vtp.position.xyz.xy));
	
	Surface = TransparencyTransparency_Sample1.a;//2
	
	
			//Generate Layer: Transparency6
				//Sample parts of the layer:
					half4 Transparency6Transparency_Sample1 = 0.5294118;
	
				//Apply Effects:
					Transparency6Transparency_Sample1.a = 1;
					Transparency6Transparency_Sample1.a = (Transparency6Transparency_Sample1.a*(saturate((vd.screenPos.z-(0))/(1.92-0))));
	
	Surface = lerp(Surface,1,Transparency6Transparency_Sample1.a);//1
	
	
	
	return float4(outputColor.rgb,Surface);
	
}
float4 Transparency ( float4 outputColor){
	outputColor.a -= _Fade_Distance;
	clip(outputColor.a);
	return outputColor;
	
}
VertexToPixel Vertex (VertexShaderInput v){
	VertexToPixel vtp;
	UNITY_INITIALIZE_OUTPUT(VertexToPixel,vtp);
	VertexData vd;
	UNITY_INITIALIZE_OUTPUT(VertexData,vd);
	vd.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	vd.position = UnityObjectToClipPos(v.vertex);
	vd.worldNormal = UnityObjectToWorldNormalNew(v.normal);
	vd.worldTangent = UnityObjectToWorldNormalNew(v.tangent);
	vd.worldBitangent = cross(vd.worldNormal, vd.worldTangent) * v.tangent.w * unity_WorldTransformParams.w;
	vd.screenPos = ComputeScreenPos (UnityObjectToClipPos (v.vertex)).xyw;
	vd.worldViewDir = normalize(UnityWorldSpaceViewDir(vd.worldPos));
	vd.TtoWSpaceX = float4(vd.worldTangent.x, vd.worldBitangent.x, vd.worldNormal.x, vd.worldPos.x);
	vd.TtoWSpaceY = float4(vd.worldTangent.y, vd.worldBitangent.y, vd.worldNormal.y, vd.worldPos.y);
	vd.TtoWSpaceZ = float4(vd.worldTangent.z, vd.worldBitangent.z, vd.worldNormal.z, vd.worldPos.z);
	vd.uv_Albedo_GLTexture = TRANSFORM_TEX(v.texcoord, _Albedo_GLTexture);
	vd.uv_Normal_Map_GLTexture = TRANSFORM_TEX(v.texcoord, _Normal_Map_GLTexture);
	vd.uv_Metalness_2_GLTexture = TRANSFORM_TEX(v.texcoord, _Metalness_2_GLTexture);
	#if UNITY_SHOULD_SAMPLE_SH
	vd.sh = 0;
// SH/ambient and vertex lights
#ifdef LIGHTMAP_OFF
	vd.sh = 0;
	// Approximated illumination from non-important point lights
	#ifdef VERTEXLIGHT_ON
		vd.sh += Shade4PointLights (
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, vd.worldPos, vd.worldNormal);
	#endif
	vd.sh = ShadeSHPerVertex (vd.worldNormal, vd.sh);
#endif // LIGHTMAP_OFF
;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
	vd.lmap = 0;
#ifndef DYNAMICLIGHTMAP_OFF
	vd.lmap.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
#endif
#ifndef LIGHTMAP_OFF
	vd.lmap.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
#endif
;
	#endif
	
	
	
	
	vtp.position = vd.position;
	vtp.screenPos = vd.screenPos;
	vtp.worldViewDir = vd.worldViewDir;
	vtp.TtoWSpaceX = vd.TtoWSpaceX;
	vtp.TtoWSpaceY = vd.TtoWSpaceY;
	vtp.TtoWSpaceZ = vd.TtoWSpaceZ;
	vtp.uv_Albedo_GLTexture = vd.uv_Albedo_GLTexture;
	vtp.uv_Normal_Map_GLTexture = vd.uv_Normal_Map_GLTexture;
	vtp.uv_Metalness_2_GLTexture = vd.uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
	vtp.sh = vd.sh;
#endif
	#define pos position
	TRANSFER_SHADOW(vtp);
	UNITY_TRANSFER_FOG(vtp,vtp.pos);
#undef pos
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
	vtp.lmap = vd.lmap;
	#endif
	return vtp;
}
			half4 Pixel (VertexToPixel vtp) : SV_Target {
				half4 outputColor = half4(0,0,0,0);
				half3 outputNormal = half3(0,0,1);
				half3 depth = half3(0,0,0);//Tangent Corrected depth, World Space depth, Normalized depth
				half3 tempDepth = half3(0,0,0);
				VertexData vd;
				UNITY_INITIALIZE_OUTPUT(VertexData,vd);
				vd.worldPos = half3(vtp.TtoWSpaceX.w,vtp.TtoWSpaceY.w,vtp.TtoWSpaceZ.w);
				vd.worldNormal = half3(vtp.TtoWSpaceX.z,vtp.TtoWSpaceY.z,vtp.TtoWSpaceZ.z);
				vd.worldTangent = half3(vtp.TtoWSpaceX.x,vtp.TtoWSpaceY.x,vtp.TtoWSpaceZ.x);
				vd.screenPos = float3(vtp.screenPos.xy/vtp.screenPos.z,vtp.screenPos.z);
				vd.worldViewDir = normalize(UnityWorldSpaceViewDir(vd.worldPos));
				vd.worldRefl = reflect(-vd.worldViewDir, vd.worldNormal);
				vd.TtoWSpaceX = vtp.TtoWSpaceX;
				vd.TtoWSpaceY = vtp.TtoWSpaceY;
				vd.TtoWSpaceZ = vtp.TtoWSpaceZ;
				vd.uv_Albedo_GLTexture = vtp.uv_Albedo_GLTexture;
				vd.uv_Normal_Map_GLTexture = vtp.uv_Normal_Map_GLTexture;
				vd.uv_Metalness_2_GLTexture = vtp.uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
				vd.sh = vtp.sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
				vd.lmap = vtp.lmap;
	#endif
				half4 previousBaseColor = 0;//Honestly just a quick hack to get Metal specular working XD
				half OneMinusAlpha = 0; //An optimization to avoid redundant 1-a if already calculated in ingredient function :)
				outputNormal = vd.worldNormal;
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT(UnityGI,gi);
// Setup lighting environment
				#ifndef USING_DIRECTIONAL_LIGHT
					fixed3 lightDir = normalize(UnityWorldSpaceLightDir(vd.worldPos));
				#else
					fixed3 lightDir = _WorldSpaceLightPos0.xyz;
				#endif
				//Uses SHADOW_COORDS
				UNITY_LIGHT_ATTENUATION(atten, vtp, vd.worldPos)
				vd.Atten = atten;
				gi.indirect.diffuse = 0;
				gi.indirect.specular = 0;
				#if !defined(LIGHTMAP_ON)
					gi.light.color = _LightColor0.rgb;
					gi.light.dir = lightDir;
					gi.light.ndotl = LambertTerm (vd.worldNormal, gi.light.dir);
				#endif
				// Call GI (lightmaps/SH/reflections) lighting function
				UnityGIInput giInput;
				UNITY_INITIALIZE_OUTPUT(UnityGIInput, giInput);
				giInput.light = gi.light;
				giInput.worldPos = vd.worldPos;
				giInput.worldViewDir = vd.worldViewDir;
				giInput.atten = atten;
				#if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
					giInput.lightmapUV = vd.lmap;
				#else
					giInput.lightmapUV = 0.0;
				#endif
				#if UNITY_SHOULD_SAMPLE_SH
					giInput.ambient = vd.sh;
				#else
					giInput.ambient.rgb = 0.0;
				#endif
				giInput.probeHDR[0] = unity_SpecCube0_HDR;
				giInput.probeHDR[1] = unity_SpecCube1_HDR;
				#if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
					giInput.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
				#endif
				#if UNITY_SPECCUBE_BOX_PROJECTION
					giInput.boxMax[0] = unity_SpecCube0_BoxMax;
					giInput.probePosition[0] = unity_SpecCube0_ProbePosition;
					giInput.boxMax[1] = unity_SpecCube1_BoxMax;
					giInput.boxMin[1] = unity_SpecCube1_BoxMin;
					giInput.probePosition[1] = unity_SpecCube1_ProbePosition;
				#endif
				half3 outputTangent_Normals = Tangent_Normals ( vd);
				outputNormal = outputTangent_Normals.rgb;//0
								vd.worldNormal = outputNormal;
				vd.worldRefl = reflect(-vd.worldViewDir, vd.worldNormal);
				half4 outputDiffuse = Diffuse ( vd, gi, giInput, previousBaseColor);
				outputColor = half4(outputDiffuse.rgb,1);//7
								half4 outputUnlit = Unlit ( vd);
				outputColor = half4((outputColor.rgb + outputUnlit.rgb),1) ;//0
								half4 outputSpecular = Specular ( vd, gi, giInput, previousBaseColor, OneMinusAlpha);
				outputColor = half4(outputSpecular.rgb,1) ;//0
								half4 outputSet_Alpha_Channel = Set_Alpha_Channel ( vd, vtp, outputColor);
				outputColor = outputSet_Alpha_Channel;//10
								outputColor = Transparency ( outputColor);
				UNITY_APPLY_FOG(vtp.fogCoord, outputColor); // apply fog (UNITY_FOG_COORDS));
				return outputColor;

			}
		ENDCG
	}
AlphaToMask Off
	Pass {
		Name "DEFERRED"
		Tags { "LightMode" = "Deferred" }
	ZTest LEqual
	ZWrite On
	Blend Off//No transparency
	Cull Back//Culling specifies which sides of the models faces to hide.

		
		CGPROGRAM
			// compile directives
				#pragma vertex Vertex
				#pragma fragment Pixel
				#pragma target 2.5
				#pragma multi_compile __ UNITY_COLORSPACE_GAMMA
				#pragma exclude_renderers nomrt
				#pragma multi_compile_prepassfinal
				#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
				#include "HLSLSupport.cginc"
				#include "UnityShaderVariables.cginc"
				#define UNITY_PASS_DEFERRED
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "UnityPBSLighting.cginc"
				#include "AutoLight.cginc"

				#define INTERNAL_DATA
				#define WorldReflectionVector(data,normal) data.worldRefl
				#define WorldNormalVector(data,normal) normal
			//Make our inputs accessible by declaring them here.
				sampler2D _Albedo_GLTexture;
float4 _Albedo_GLTexture_ST;
float4 _Albedo_GLTexture_HDR;
				sampler2D _Normal_Map_GLTexture;
float4 _Normal_Map_GLTexture_ST;
float4 _Normal_Map_GLTexture_HDR;
				float _Smoothness;
				float _Light_Size;
				sampler2D _Metalness_2_GLTexture;
float4 _Metalness_2_GLTexture_ST;
float4 _Metalness_2_GLTexture_HDR;
				float _Fade_Distance;
				float _Transparency_GLFill;
				float _Metalness_GLNumber;
				float4 _Emission_GLColor_A;
				float _Albedo_2_Copy_GLMix_Amount;
				float _Emission_GLPower;
				float _Emission_GLMultiply;
				float _Normal_Map_GLHeight;
				float _Emission_GLMultiply_2;
				float _Metalness_2_GLPower;
				sampler2D SSTEMPSV12816594;//Texture
				float4 SSTEMPSV12816594_HDR;//Texture

#if !defined (SSUNITY_BRDF_PBS) // allow to explicitly override BRDF in custom shader
	// still add safe net for low shader models, otherwise we might end up with shaders failing to compile
	#if SHADER_TARGET < 30
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF3)
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF2)
		#define SSUNITY_BRDF_PBS 2
	#elif defined(UNITY_PBS_USE_BRDF1)
		#define SSUNITY_BRDF_PBS 1
	#elif defined(SHADER_TARGET_SURFACE_ANALYSIS)
		// we do preprocess pass during shader analysis and we dont actually care about brdf as we need only inputs/outputs
		#define SSUNITY_BRDF_PBS 1
	#else
		#error something broke in auto-choosing BRDF (Shader Sandwich)
	#endif
#endif

	//From UnityCG.inc, Unity 2017.01 - works better than any of the earlier ones
	inline float3 UnityObjectToWorldNormalNew( in float3 norm ){
		#ifdef UNITY_ASSUME_UNIFORM_SCALING
			return UnityObjectToWorldDir(norm);
		#else
			// mul(IT_M, norm) => mul(norm, I_M) => {dot(norm, I_M.col0), dot(norm, I_M.col1), dot(norm, I_M.col2)}
			return normalize(mul(norm, (float3x3)unity_WorldToObject));
		#endif
	}
	float4 GammaToLinear(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 GammaToLinearForce(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 LinearToGamma(float4 col){
		return col;
	}

	float4 LinearToGammaForWeirdSituations(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinear(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinearForce(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 LinearToGamma(float3 col){
		return col;
	}

	float GammaToLinear(float col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}



















	float RandomNoise3D(float3 co){
		return frac(sin( dot(co.xyz ,float3(12.9898,78.233,45.5432) )) * 43758.5453);
	}
	float RandomNoise2D(float2 co){
		return frac(sin( dot(co.xy ,float2(12.9898,78.233) )) * 43758.5453);
	}
 





struct VertexShaderInput{
	float4 vertex : POSITION;
	float4 tangent : TANGENT;
	float3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
	float4 texcoord1 : TEXCOORD1;
	float4 texcoord2 : TEXCOORD2;
};
struct VertexToPixel{
	float4 position : POSITION;
	float3 screenPos : TEXCOORD0;
	float4 TtoWSpaceX : TEXCOORD1;
	float4 TtoWSpaceY : TEXCOORD2;
	float4 TtoWSpaceZ : TEXCOORD3;
	float2 genericTexcoord : TEXCOORD4;
	float2 uv_Albedo_GLTexture : TEXCOORD5;
	float2 uv_Normal_Map_GLTexture : TEXCOORD6;
	float2 uv_Metalness_2_GLTexture : TEXCOORD7;
	#if UNITY_SHOULD_SAMPLE_SH
		float3 sh : TEXCOORD8;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
		float4 lmap : TEXCOORD9;
	#endif
};

			struct DeferredOutput{
				half3 Emission;
				half4 SpecSmoothness;
				half3 Albedo;
				half Alpha;
				half  Occlusion;
				half3 Normal;
				half3 AmbientDiffuse;
			};
			
struct VertexData{
	float3 worldPos;
	float4 position;
	float3 worldNormal;
	float3 worldTangent;
	float3 worldBitangent;
	float3 screenPos;
	float3 worldViewDir;
	float4 TtoWSpaceX;
	float4 TtoWSpaceY;
	float4 TtoWSpaceZ;
	float2 genericTexcoord;
	float2 uv_Albedo_GLTexture;
	float2 uv_Normal_Map_GLTexture;
	float2 uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
		float3 sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
		float4 lmap;
	#endif
	float Atten;
};
//OutputPremultiplied: False
//UseAlphaGenerate: False
half3 Tangent_Normals ( VertexData vd, DeferredOutput deferredOut){
	half3 Surface = half3(0,0,1);
		//Generate layers for the Normals channel.
			//Generate Layer: Normal Map
				//Sample parts of the layer:
					half4 Normal_MapNormals_Sample2 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0.001666666, 0)));
					half4 Normal_MapNormals_Sample3 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0, 0.001666666)));
					half4 Normal_MapNormals_Sample1 = tex2D(_Normal_Map_GLTexture,vd.uv_Normal_Map_GLTexture);
	
				//Apply Effects:
					Normal_MapNormals_Sample1.rgb = normalize(float3(((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample2.a)*_Normal_Map_GLHeight),((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample3.a)*_Normal_Map_GLHeight),1));
	
	Surface = Normal_MapNormals_Sample1.rgb;//0
	
	
	return normalize(half3(dot(vd.TtoWSpaceX.xyz, Surface),dot(vd.TtoWSpaceY.xyz, Surface),dot(vd.TtoWSpaceZ.xyz, Surface)));
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
DeferredOutput Diffuse ( VertexData vd, UnityGI gi, UnityGIInput giInput, inout half4 previousBaseColor, DeferredOutput deferredOut, out half OneMinusAlpha){
	half4 Surface = half4(0,0,0,0);
		//Generate layers for the Albedo channel.
			//Generate Layer: Albedo 2
				//Sample parts of the layer:
					half4 Albedo_2Albedo_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = half4(Albedo_2Albedo_Sample1.rgb,1);//7
	
	
	previousBaseColor = Surface;
	Unity_GlossyEnvironmentData g;
	g.roughness = (1-_Smoothness);
	g.reflUVW = vd.worldViewDir;
	gi = UnityGlobalIllumination(giInput, 1, vd.worldNormal,g);
	deferredOut.Albedo = Surface.rgb;
	OneMinusAlpha = 0;
	deferredOut.Alpha = Surface.a;
	deferredOut.AmbientDiffuse = gi.indirect.diffuse;
	OneMinusAlpha = 1-Surface.a;
	return deferredOut;
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
DeferredOutput Unlit ( VertexData vd, DeferredOutput deferredOut, out half OneMinusAlpha){
	half4 Surface = half4(0.8,0.8,0.8,1.0);
		//Generate layers for the Unlit channel.
			//Generate Layer: Unlit2
				//Sample parts of the layer:
					half4 Unlit2Surface_Sample1 = GammaToLinear(float4(0, 0, 0, 1));
	
	Surface = half4(Unlit2Surface_Sample1.rgb,1) ;//0
	
	
			//Generate Layer: Albedo 2 Copy
				//Sample parts of the layer:
					half4 Albedo_2_CopySurface_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = lerp(Surface,half4(Albedo_2_CopySurface_Sample1.rgb, 1),_Albedo_2_Copy_GLMix_Amount);//2
	
	
	OneMinusAlpha = 0;
	deferredOut.Alpha = 1;
	deferredOut.Emission = Surface;
	return deferredOut;
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
DeferredOutput Specular ( VertexData vd, inout half4 previousBaseColor, DeferredOutput deferredOut, out half OneMinusAlpha){
	half Metalness = 0;//Metallic Mode
		//Generate layers for the Metalness channel.
			//Generate Layer: Metalness 2
				//Sample parts of the layer:
					half4 Metalness_2Specular_Sample1 = tex2D(_Metalness_2_GLTexture,vd.uv_Metalness_2_GLTexture);
	
				//Apply Effects:
					Metalness_2Specular_Sample1.rgb = pow(Metalness_2Specular_Sample1.rgb,_Metalness_2_GLPower);
	
	Metalness = lerp(Metalness,(Metalness + Metalness_2Specular_Sample1.a),Metalness_2Specular_Sample1.a);//1
	
	
			//Generate Layer: Metalness
				//Sample parts of the layer:
					half4 MetalnessSpecular_Sample1 = _Metalness_GLNumber;
	
	Metalness = (Metalness - MetalnessSpecular_Sample1.a);//2
	
	
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	half oneMinusReflectivity = OneMinusReflectivityFromMetallic(Metalness);
	half reflectivity = 1-oneMinusReflectivity;
	OneMinusAlpha = oneMinusReflectivity;
	deferredOut.Alpha = 1-OneMinusAlpha;
	deferredOut.SpecSmoothness = half4(Surface,_Smoothness);
	return deferredOut;
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
DeferredOutput Emission ( VertexData vd, DeferredOutput deferredOut, out half OneMinusAlpha){
	half4 Surface = half4(0,0,0,0);
		//Generate layers for the Emission channel.
			//Generate Layer: Emission2
				//Sample parts of the layer:
					half4 Emission2Emission_Sample1 = GammaToLinear(float4(0, 0, 0, 1));
	
	Surface = half4(Emission2Emission_Sample1.rgb,1) ;//0
	
	
			//Generate Layer: Emission
				//Sample parts of the layer:
					half4 EmissionEmission_Sample1 = tex2D(SSTEMPSV12816594,vd.genericTexcoord);
	
				//Apply Effects:
					EmissionEmission_Sample1.rgb = (EmissionEmission_Sample1.rgb*_Emission_GLMultiply_2);
	
	Surface = half4((Surface.rgb + EmissionEmission_Sample1.rgb),1) ;//0
	
	
			//Generate Layer: Emission 2
				//Sample parts of the layer:
					half4 Emission_2Emission_Sample1 = lerp(GammaToLinear(float4(0, 0, 0, 0)), _Emission_GLColor_A, GammaToLinear((1-dot(vd.worldNormal, vd.worldViewDir))));
	
				//Apply Effects:
					Emission_2Emission_Sample1.rgb = pow(Emission_2Emission_Sample1.rgb,_Emission_GLPower);
					Emission_2Emission_Sample1.rgb = (Emission_2Emission_Sample1.rgb*_Emission_GLMultiply);
	
	Surface = half4((Surface.rgb + Emission_2Emission_Sample1.rgb),1) ;//0
	
	
	
	deferredOut.Alpha = Surface.a;
	OneMinusAlpha = 0;
	deferredOut.Emission = Surface;
	return deferredOut;
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: True
DeferredOutput Set_Alpha_Channel ( VertexData vd, VertexToPixel vtp, DeferredOutput deferredOut){
	float Surface = 0;
		//Generate layers for the Transparency channel.
			//Generate Layer: Transparency
				//Sample parts of the layer:
					half4 TransparencyTransparency_Sample1 = GammaToLinear(RandomNoise2D(vtp.position.xyz.xy));
	
	Surface = TransparencyTransparency_Sample1.a;//2
	
	
			//Generate Layer: Transparency6
				//Sample parts of the layer:
					half4 Transparency6Transparency_Sample1 = 0.5294118;
	
				//Apply Effects:
					Transparency6Transparency_Sample1.a = 1;
					Transparency6Transparency_Sample1.a = (Transparency6Transparency_Sample1.a*(saturate((vd.screenPos.z-(0))/(1.92-0))));
	
	Surface = lerp(Surface,1,Transparency6Transparency_Sample1.a);//1
	
	
	
	deferredOut.Alpha = Surface;
	return deferredOut;
	
}
DeferredOutput Transparency ( DeferredOutput deferredOut){
	deferredOut.Alpha -= _Fade_Distance;
	clip(deferredOut.Alpha);
	return deferredOut;
	
}
VertexToPixel Vertex (VertexShaderInput v){
	VertexToPixel vtp;
	UNITY_INITIALIZE_OUTPUT(VertexToPixel,vtp);
	VertexData vd;
	UNITY_INITIALIZE_OUTPUT(VertexData,vd);
	vd.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	vd.position = UnityObjectToClipPos(v.vertex);
	vd.worldNormal = UnityObjectToWorldNormalNew(v.normal);
	vd.worldTangent = UnityObjectToWorldNormalNew(v.tangent);
	vd.worldBitangent = cross(vd.worldNormal, vd.worldTangent) * v.tangent.w * unity_WorldTransformParams.w;
	vd.screenPos = ComputeScreenPos (UnityObjectToClipPos (v.vertex)).xyw;
	vd.TtoWSpaceX = float4(vd.worldTangent.x, vd.worldBitangent.x, vd.worldNormal.x, vd.worldPos.x);
	vd.TtoWSpaceY = float4(vd.worldTangent.y, vd.worldBitangent.y, vd.worldNormal.y, vd.worldPos.y);
	vd.TtoWSpaceZ = float4(vd.worldTangent.z, vd.worldBitangent.z, vd.worldNormal.z, vd.worldPos.z);
	vd.genericTexcoord = v.texcoord;
	vd.uv_Albedo_GLTexture = TRANSFORM_TEX(v.texcoord, _Albedo_GLTexture);
	vd.uv_Normal_Map_GLTexture = TRANSFORM_TEX(v.texcoord, _Normal_Map_GLTexture);
	vd.uv_Metalness_2_GLTexture = TRANSFORM_TEX(v.texcoord, _Metalness_2_GLTexture);
	#if UNITY_SHOULD_SAMPLE_SH
	vd.sh = 0;
// SH/ambient and vertex lights
#ifdef LIGHTMAP_OFF
	vd.sh = 0;
	// Approximated illumination from non-important point lights
	#ifdef VERTEXLIGHT_ON
		vd.sh += Shade4PointLights (
		unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
		unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
		unity_4LightAtten0, vd.worldPos, vd.worldNormal);
	#endif
	vd.sh = ShadeSHPerVertex (vd.worldNormal, vd.sh);
#endif // LIGHTMAP_OFF
;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
	vd.lmap = 0;
#ifndef DYNAMICLIGHTMAP_OFF
	vd.lmap.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
#endif
#ifndef LIGHTMAP_OFF
	vd.lmap.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
#endif
;
	#endif
	
	
	
	
	vtp.position = vd.position;
	vtp.screenPos = vd.screenPos;
	vtp.TtoWSpaceX = vd.TtoWSpaceX;
	vtp.TtoWSpaceY = vd.TtoWSpaceY;
	vtp.TtoWSpaceZ = vd.TtoWSpaceZ;
	vtp.genericTexcoord = vd.genericTexcoord;
	vtp.uv_Albedo_GLTexture = vd.uv_Albedo_GLTexture;
	vtp.uv_Normal_Map_GLTexture = vd.uv_Normal_Map_GLTexture;
	vtp.uv_Metalness_2_GLTexture = vd.uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
	vtp.sh = vd.sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
	vtp.lmap = vd.lmap;
	#endif
	return vtp;
}
			void Pixel (VertexToPixel vtp,
				out half4 outDiffuse : SV_Target0,
				out half4 outSpecSmoothness : SV_Target1,
				out half4 outNormal : SV_Target2,
				out half4 outEmission : SV_Target3) {
					outDiffuse = 0;
					outSpecSmoothness = 0;
					outNormal = 0;
					outEmission = 0;
					half3 depth = half3(0,0,0);//Tangent Corrected depth, World Space depth, Normalized depth
					half3 tempDepth = half3(0,0,0);
					VertexData vd;
						UNITY_INITIALIZE_OUTPUT(VertexData,vd);
					DeferredOutput deferredOut;
						UNITY_INITIALIZE_OUTPUT(DeferredOutput,deferredOut);
						deferredOut.Occlusion = 1;
				vd.worldPos = half3(vtp.TtoWSpaceX.w,vtp.TtoWSpaceY.w,vtp.TtoWSpaceZ.w);
				vd.worldNormal = half3(vtp.TtoWSpaceX.z,vtp.TtoWSpaceY.z,vtp.TtoWSpaceZ.z);
				vd.screenPos = float3(vtp.screenPos.xy/vtp.screenPos.z,vtp.screenPos.z);
				vd.worldViewDir = normalize(UnityWorldSpaceViewDir(vd.worldPos));
				vd.TtoWSpaceX = vtp.TtoWSpaceX;
				vd.TtoWSpaceY = vtp.TtoWSpaceY;
				vd.TtoWSpaceZ = vtp.TtoWSpaceZ;
				vd.genericTexcoord = vtp.genericTexcoord;
				vd.uv_Albedo_GLTexture = vtp.uv_Albedo_GLTexture;
				vd.uv_Normal_Map_GLTexture = vtp.uv_Normal_Map_GLTexture;
				vd.uv_Metalness_2_GLTexture = vtp.uv_Metalness_2_GLTexture;
	#if UNITY_SHOULD_SAMPLE_SH
				vd.sh = vtp.sh;
#endif
	#if (!defined(LIGHTMAP_OFF))||(SHADER_TARGET >= 30)
				vd.lmap = vtp.lmap;
	#endif
				half4 previousBaseColor = 0;//Honestly just a quick hack to get Metal specular working XD
				half OneMinusAlpha = 0; //An optimization to avoid redundant 1-a if already calculated in ingredient function :)
				deferredOut.Normal = vd.worldNormal;
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT(UnityGI,gi);
// Setup lighting environment
				#ifndef USING_DIRECTIONAL_LIGHT
					fixed3 lightDir = normalize(UnityWorldSpaceLightDir(vd.worldPos));
				#else
					fixed3 lightDir = _WorldSpaceLightPos0.xyz;
				#endif
				half atten = 1;
				vd.Atten = atten;
				gi.indirect.diffuse = 0;
				gi.indirect.specular = 0;
				gi.light.color = 0;
				gi.light.dir = half3(0,1,0);
				gi.light.ndotl = 0;				// Call GI (lightmaps/SH/reflections) lighting function
				UnityGIInput giInput;
				UNITY_INITIALIZE_OUTPUT(UnityGIInput, giInput);
				giInput.light = gi.light;
				giInput.worldPos = vd.worldPos;
				giInput.worldViewDir = vd.worldViewDir;
				giInput.atten = atten;
				#if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
					giInput.lightmapUV = vd.lmap;
				#else
					giInput.lightmapUV = 0.0;
				#endif
				#if UNITY_SHOULD_SAMPLE_SH
					giInput.ambient = vd.sh;
				#else
					giInput.ambient.rgb = 0.0;
				#endif
				giInput.probeHDR[0] = unity_SpecCube0_HDR;
				giInput.probeHDR[1] = unity_SpecCube1_HDR;
				#if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
					giInput.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
				#endif
				#if UNITY_SPECCUBE_BOX_PROJECTION
					giInput.boxMax[0] = unity_SpecCube0_BoxMax;
					giInput.probePosition[0] = unity_SpecCube0_ProbePosition;
					giInput.boxMax[1] = unity_SpecCube1_BoxMax;
					giInput.boxMin[1] = unity_SpecCube1_BoxMin;
					giInput.probePosition[1] = unity_SpecCube1_ProbePosition;
				#endif
				half3 outputTangent_Normals = Tangent_Normals ( vd, deferredOut);
				deferredOut.Normal.xyz = outputTangent_Normals.rgb;//0
								vd.worldNormal = deferredOut.Normal.xyz;
				DeferredOutput deferredOutDiffuse = Diffuse ( vd, gi, giInput, previousBaseColor, deferredOut, OneMinusAlpha);
				deferredOut.Albedo.rgb = deferredOutDiffuse.Albedo.rgb.rgb;//2
								deferredOut.SpecSmoothness*=OneMinusAlpha;
				deferredOut.Emission*=OneMinusAlpha;
				deferredOut.Alpha = deferredOutDiffuse.Alpha.rrrr.a;//2
								deferredOut.AmbientDiffuse.rgb = deferredOutDiffuse.AmbientDiffuse.rgb.rgb;//2
								DeferredOutput deferredOutUnlit = Unlit ( vd, deferredOut, OneMinusAlpha);
				deferredOut.Alpha = (deferredOut.Alpha + deferredOutUnlit.Alpha.rrrr.a);//0
								deferredOut.Emission.rgb = (deferredOut.Emission.rgb + deferredOutUnlit.Emission.rgb.rgb);//0
								DeferredOutput deferredOutSpecular = Specular ( vd, previousBaseColor, deferredOut, OneMinusAlpha);
				deferredOut.Alpha = 1;//0
								deferredOut.SpecSmoothness.rgba = deferredOutSpecular.SpecSmoothness.rgba;//2
								deferredOut.Albedo*=OneMinusAlpha;
				DeferredOutput deferredOutEmission = Emission ( vd, deferredOut, OneMinusAlpha);
				deferredOut.Alpha = (deferredOut.Alpha + deferredOutEmission.Alpha.rrrr.a);//0
								deferredOut.Emission.rgb = (deferredOut.Emission.rgb + deferredOutEmission.Emission.rgb.rgb);//0
								DeferredOutput deferredOutSet_Alpha_Channel = Set_Alpha_Channel ( vd, vtp, deferredOut);
				deferredOut.Alpha = deferredOutSet_Alpha_Channel.Alpha.rrrr.a;//2
								deferredOut.Alpha = Transparency ( deferredOut).Alpha;
				outDiffuse = half4(deferredOut.Albedo,deferredOut.Occlusion);
				outSpecSmoothness = deferredOut.SpecSmoothness;
				outNormal.xyz = deferredOut.Normal * 0.5 + 0.5;
				outEmission.rgb = deferredOut.Emission + deferredOut.Albedo * deferredOut.AmbientDiffuse;
				#ifndef UNITY_HDR_ON
					outEmission.rgb = exp2(-outEmission.rgb);
				#endif

			}
		ENDCG
	}
AlphaToMask Off
	Pass {
		Name "ShadowCaster"
		Tags { "LightMode" = "ShadowCaster" }
	ZTest LEqual
	ZWrite On
	Blend Off//No transparency
	Cull Back//Culling specifies which sides of the models faces to hide.

		
		CGPROGRAM
			// compile directives
				#pragma vertex Vertex
				#pragma fragment Pixel
				#pragma target 2.5
				#pragma multi_compile_fog
				#pragma multi_compile __ UNITY_COLORSPACE_GAMMA
				#pragma multi_compile_shadowcaster
				#include "HLSLSupport.cginc"
				#include "UnityShaderVariables.cginc"
				#define SHADERSANDWICH_SHADOWCASTER
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "UnityPBSLighting.cginc"
				#include "AutoLight.cginc"

				#define INTERNAL_DATA
				#define WorldReflectionVector(data,normal) data.worldRefl
				#define WorldNormalVector(data,normal) normal
			//Make our inputs accessible by declaring them here.
				sampler2D _Albedo_GLTexture;
float4 _Albedo_GLTexture_ST;
float4 _Albedo_GLTexture_HDR;
				sampler2D _Normal_Map_GLTexture;
float4 _Normal_Map_GLTexture_ST;
float4 _Normal_Map_GLTexture_HDR;
				float _Smoothness;
				float _Light_Size;
				sampler2D _Metalness_2_GLTexture;
float4 _Metalness_2_GLTexture_ST;
float4 _Metalness_2_GLTexture_HDR;
				float _Fade_Distance;
				float _Transparency_GLFill;
				float _Metalness_GLNumber;
				float4 _Emission_GLColor_A;
				float _Albedo_2_Copy_GLMix_Amount;
				float _Emission_GLPower;
				float _Emission_GLMultiply;
				float _Normal_Map_GLHeight;
				float _Emission_GLMultiply_2;
				float _Metalness_2_GLPower;

#if !defined (SSUNITY_BRDF_PBS) // allow to explicitly override BRDF in custom shader
	// still add safe net for low shader models, otherwise we might end up with shaders failing to compile
	#if SHADER_TARGET < 30
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF3)
		#define SSUNITY_BRDF_PBS 3
	#elif defined(UNITY_PBS_USE_BRDF2)
		#define SSUNITY_BRDF_PBS 2
	#elif defined(UNITY_PBS_USE_BRDF1)
		#define SSUNITY_BRDF_PBS 1
	#elif defined(SHADER_TARGET_SURFACE_ANALYSIS)
		// we do preprocess pass during shader analysis and we dont actually care about brdf as we need only inputs/outputs
		#define SSUNITY_BRDF_PBS 1
	#else
		#error something broke in auto-choosing BRDF (Shader Sandwich)
	#endif
#endif

	//From UnityCG.inc, Unity 2017.01 - works better than any of the earlier ones
	inline float3 UnityObjectToWorldNormalNew( in float3 norm ){
		#ifdef UNITY_ASSUME_UNIFORM_SCALING
			return UnityObjectToWorldDir(norm);
		#else
			// mul(IT_M, norm) => mul(norm, I_M) => {dot(norm, I_M.col0), dot(norm, I_M.col1), dot(norm, I_M.col2)}
			return normalize(mul(norm, (float3x3)unity_WorldToObject));
		#endif
	}
	float4 GammaToLinear(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 GammaToLinearForce(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float4 LinearToGamma(float4 col){
		return col;
	}

	float4 LinearToGammaForWeirdSituations(float4 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			col.rgb = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinear(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 GammaToLinearForce(float3 col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}

	float3 LinearToGamma(float3 col){
		return col;
	}

	float GammaToLinear(float col){
		#if defined(UNITY_COLORSPACE_GAMMA)
			//Best programming evar XD
		#else
			col = pow(col,2.2);
		#endif
		return col;
	}



















	float RandomNoise3D(float3 co){
		return frac(sin( dot(co.xyz ,float3(12.9898,78.233,45.5432) )) * 43758.5453);
	}
	float RandomNoise2D(float2 co){
		return frac(sin( dot(co.xy ,float2(12.9898,78.233) )) * 43758.5453);
	}
 





struct VertexShaderInput{
	float4 vertex : POSITION;
	float4 tangent : TANGENT;
	float3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
};
struct VertexToPixel{
	float4 position : POSITION;
	float3 screenPos : TEXCOORD0;
	float4 TtoWSpaceX : TEXCOORD1;
	float4 TtoWSpaceY : TEXCOORD2;
	float4 TtoWSpaceZ : TEXCOORD3;
	float2 uv_Albedo_GLTexture : TEXCOORD4;
	float2 uv_Normal_Map_GLTexture : TEXCOORD5;
	float2 uv_Metalness_2_GLTexture : TEXCOORD6;
	#define pos position
		UNITY_FOG_COORDS(7)
#undef pos
	#ifdef SHADOWS_CUBE
		float3 vec : TEXCOORD8;
#endif
	#ifndef SHADOWS_CUBE
		#ifdef UNITY_MIGHT_NOT_HAVE_DEPTH_TEXTURE
			float4 hpos : TEXCOORD9;
		#endif
	#endif
};

struct VertexData{
	float3 worldPos;
	float4 position;
	float3 worldNormal;
	float3 worldTangent;
	float3 worldBitangent;
	float3 screenPos;
	float4 TtoWSpaceX;
	float4 TtoWSpaceY;
	float4 TtoWSpaceZ;
	float2 uv_Albedo_GLTexture;
	float2 uv_Normal_Map_GLTexture;
	float2 uv_Metalness_2_GLTexture;
	#ifdef SHADOWS_CUBE
		float3 vec;
#endif
	#ifndef SHADOWS_CUBE
		#ifdef UNITY_MIGHT_NOT_HAVE_DEPTH_TEXTURE
			float4 hpos;
		#endif
	#endif
	float Atten;
};
//OutputPremultiplied: False
//UseAlphaGenerate: False
half3 Tangent_Normals ( VertexData vd){
	half3 Surface = half3(0,0,1);
		//Generate layers for the Normals channel.
			//Generate Layer: Normal Map
				//Sample parts of the layer:
					half4 Normal_MapNormals_Sample2 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0.001666666, 0)));
					half4 Normal_MapNormals_Sample3 = tex2D(_Normal_Map_GLTexture,(vd.uv_Normal_Map_GLTexture + float2(0, 0.001666666)));
					half4 Normal_MapNormals_Sample1 = tex2D(_Normal_Map_GLTexture,vd.uv_Normal_Map_GLTexture);
	
				//Apply Effects:
					Normal_MapNormals_Sample1.rgb = normalize(float3(((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample2.a)*_Normal_Map_GLHeight),((Normal_MapNormals_Sample1.a-Normal_MapNormals_Sample3.a)*_Normal_Map_GLHeight),1));
	
	Surface = Normal_MapNormals_Sample1.rgb;//0
	
	
	return normalize(half3(dot(vd.TtoWSpaceX.xyz, Surface),dot(vd.TtoWSpaceY.xyz, Surface),dot(vd.TtoWSpaceZ.xyz, Surface)));
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Diffuse ( VertexData vd, inout half4 previousBaseColor){
	half4 Surface = half4(0,0,0,0);
		//Generate layers for the Albedo channel.
			//Generate Layer: Albedo 2
				//Sample parts of the layer:
					half4 Albedo_2Albedo_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = half4(Albedo_2Albedo_Sample1.rgb,1);//7
	
	
	previousBaseColor = Surface;
	return Surface;
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Unlit ( VertexData vd){
	half4 Surface = half4(0.8,0.8,0.8,1.0);
		//Generate layers for the Unlit channel.
			//Generate Layer: Unlit2
				//Sample parts of the layer:
					half4 Unlit2Surface_Sample1 = GammaToLinear(float4(0, 0, 0, 1));
	
	Surface = half4(Unlit2Surface_Sample1.rgb,1) ;//0
	
	
			//Generate Layer: Albedo 2 Copy
				//Sample parts of the layer:
					half4 Albedo_2_CopySurface_Sample1 = tex2D(_Albedo_GLTexture,vd.uv_Albedo_GLTexture);
	
	Surface = lerp(Surface,half4(Albedo_2_CopySurface_Sample1.rgb, 1),_Albedo_2_Copy_GLMix_Amount);//2
	
	
	Surface.rgb = 0;
	return Surface;
}
//OutputPremultiplied: False
//UseAlphaGenerate: False
half4 Specular ( VertexData vd, inout half4 previousBaseColor){
	half Metalness = 0;//Metallic Mode
		//Generate layers for the Metalness channel.
			//Generate Layer: Metalness 2
				//Sample parts of the layer:
					half4 Metalness_2Specular_Sample1 = tex2D(_Metalness_2_GLTexture,vd.uv_Metalness_2_GLTexture);
	
				//Apply Effects:
					Metalness_2Specular_Sample1.rgb = pow(Metalness_2Specular_Sample1.rgb,_Metalness_2_GLPower);
	
	Metalness = lerp(Metalness,(Metalness + Metalness_2Specular_Sample1.a),Metalness_2Specular_Sample1.a);//1
	
	
			//Generate Layer: Metalness
				//Sample parts of the layer:
					half4 MetalnessSpecular_Sample1 = _Metalness_GLNumber;
	
	Metalness = (Metalness - MetalnessSpecular_Sample1.a);//2
	
	
	half3 Surface = lerp (unity_ColorSpaceDielectricSpec.rgb, previousBaseColor, Metalness);
	half oneMinusReflectivity = OneMinusReflectivityFromMetallic(Metalness);
	half reflectivity = 1-oneMinusReflectivity;
	return half4(Surface,reflectivity);
	
}
//OutputPremultiplied: False
//UseAlphaGenerate: True
half4 Set_Alpha_Channel ( VertexData vd, VertexToPixel vtp, half4 outputColor){
	float Surface = 0;
		//Generate layers for the Transparency channel.
			//Generate Layer: Transparency
				//Sample parts of the layer:
					half4 TransparencyTransparency_Sample1 = GammaToLinear(RandomNoise2D(vtp.position.xyz.xy));
	
	Surface = TransparencyTransparency_Sample1.a;//2
	
	
			//Generate Layer: Transparency6
				//Sample parts of the layer:
					half4 Transparency6Transparency_Sample1 = 0.5294118;
	
				//Apply Effects:
					Transparency6Transparency_Sample1.a = 1;
					Transparency6Transparency_Sample1.a = (Transparency6Transparency_Sample1.a*(saturate((vd.screenPos.z-(0))/(1.92-0))));
	
	Surface = lerp(Surface,1,Transparency6Transparency_Sample1.a);//1
	
	
	
	return float4(outputColor.rgb,Surface);
	
}
float4 Transparency ( float4 outputColor){
	return outputColor;
	
}
VertexToPixel Vertex (VertexShaderInput v){
	VertexToPixel vtp;
	UNITY_INITIALIZE_OUTPUT(VertexToPixel,vtp);
	VertexData vd;
	UNITY_INITIALIZE_OUTPUT(VertexData,vd);
	vd.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	vd.position = 0;
	TRANSFER_SHADOW_CASTER_NOPOS(vd,vd.position);
	vd.worldNormal = UnityObjectToWorldNormalNew(v.normal);
	vd.worldTangent = UnityObjectToWorldNormalNew(v.tangent);
	vd.worldBitangent = cross(vd.worldNormal, vd.worldTangent) * v.tangent.w * unity_WorldTransformParams.w;
	vd.screenPos = ComputeScreenPos (UnityObjectToClipPos (v.vertex)).xyw;
	vd.TtoWSpaceX = float4(vd.worldTangent.x, vd.worldBitangent.x, vd.worldNormal.x, vd.worldPos.x);
	vd.TtoWSpaceY = float4(vd.worldTangent.y, vd.worldBitangent.y, vd.worldNormal.y, vd.worldPos.y);
	vd.TtoWSpaceZ = float4(vd.worldTangent.z, vd.worldBitangent.z, vd.worldNormal.z, vd.worldPos.z);
	vd.uv_Albedo_GLTexture = TRANSFORM_TEX(v.texcoord, _Albedo_GLTexture);
	vd.uv_Normal_Map_GLTexture = TRANSFORM_TEX(v.texcoord, _Normal_Map_GLTexture);
	vd.uv_Metalness_2_GLTexture = TRANSFORM_TEX(v.texcoord, _Metalness_2_GLTexture);
	
	
	
	
	vtp.position = vd.position;
	vtp.screenPos = vd.screenPos;
	vtp.TtoWSpaceX = vd.TtoWSpaceX;
	vtp.TtoWSpaceY = vd.TtoWSpaceY;
	vtp.TtoWSpaceZ = vd.TtoWSpaceZ;
	vtp.uv_Albedo_GLTexture = vd.uv_Albedo_GLTexture;
	vtp.uv_Normal_Map_GLTexture = vd.uv_Normal_Map_GLTexture;
	vtp.uv_Metalness_2_GLTexture = vd.uv_Metalness_2_GLTexture;
	#define pos position
	UNITY_TRANSFER_FOG(vtp,vtp.pos);
#undef pos
	#ifdef SHADOWS_CUBE
	vtp.vec = vd.vec;
#endif
	#ifndef SHADOWS_CUBE
		#ifdef UNITY_MIGHT_NOT_HAVE_DEPTH_TEXTURE
	vtp.hpos = vd.hpos;
		#endif
	#endif
	return vtp;
}
			half4 Pixel (VertexToPixel vtp) : SV_Target {
				half4 outputColor = half4(0,0,0,0);
				half3 outputNormal = half3(0,0,1);
				half3 depth = half3(0,0,0);//Tangent Corrected depth, World Space depth, Normalized depth
				half3 tempDepth = half3(0,0,0);
				VertexData vd;
				UNITY_INITIALIZE_OUTPUT(VertexData,vd);
				vd.worldNormal = half3(vtp.TtoWSpaceX.z,vtp.TtoWSpaceY.z,vtp.TtoWSpaceZ.z);
				vd.screenPos = float3(vtp.screenPos.xy/vtp.screenPos.z,vtp.screenPos.z);
				vd.TtoWSpaceX = vtp.TtoWSpaceX;
				vd.TtoWSpaceY = vtp.TtoWSpaceY;
				vd.TtoWSpaceZ = vtp.TtoWSpaceZ;
				vd.uv_Albedo_GLTexture = vtp.uv_Albedo_GLTexture;
				vd.uv_Normal_Map_GLTexture = vtp.uv_Normal_Map_GLTexture;
				vd.uv_Metalness_2_GLTexture = vtp.uv_Metalness_2_GLTexture;
	#ifdef SHADOWS_CUBE
				vd.vec = vtp.vec;
#endif
	#ifndef SHADOWS_CUBE
		#ifdef UNITY_MIGHT_NOT_HAVE_DEPTH_TEXTURE
				vd.hpos = vtp.hpos;
		#endif
	#endif
				half4 previousBaseColor = 0;//Honestly just a quick hack to get Metal specular working XD
				outputNormal = vd.worldNormal;
				half3 outputTangent_Normals = Tangent_Normals ( vd);
				outputNormal = outputTangent_Normals.rgb;//0
								vd.worldNormal = outputNormal;
				half4 outputDiffuse = Diffuse ( vd, previousBaseColor);
				outputColor = half4(outputDiffuse.rgb,1);//7
								half4 outputUnlit = Unlit ( vd);
				outputColor = half4((outputColor.rgb + outputUnlit.rgb),1) ;//0
								half4 outputSpecular = Specular ( vd, previousBaseColor);
				outputColor = half4(outputSpecular.rgb,1) ;//0
								half4 outputSet_Alpha_Channel = Set_Alpha_Channel ( vd, vtp, outputColor);
				outputColor = outputSet_Alpha_Channel;//10
								outputColor = Transparency ( outputColor);
				UNITY_APPLY_FOG(vtp.fogCoord, outputColor); // apply fog (UNITY_FOG_COORDS));
				SHADOW_CASTER_FRAGMENT(vd)
				return outputColor;

			}
		ENDCG
	}
}

Fallback "Legacy Shaders/Diffuse"
}


/*
Shader Sandwich Shader
	File Format Version(Float): 3.0
	Begin Shader Base

		Begin Shader Input
			Type(Text): "Image"
			VisName(Text): "Albedo - Texture"
			ImageDefault(Float): 0
			Image(Text): "bbd4c6882c708a14785e156f898327b3"
			NormalMap(Float): 0
			DefaultTexture(Text): "White"
			SeeTilingOffset(Toggle): True
			TilingOffset(Vec): 1,1,0,0
			CustomFallback(Text): "_Albedo_GLTexture"
		End Shader Input


		Begin Shader Input
			Type(Text): "Image"
			VisName(Text): "Normal Map - Texture"
			ImageDefault(Float): 0
			Image(Text): "2acb00935be0b864088c61e647c2ac60"
			NormalMap(Float): 0
			DefaultTexture(Text): "White"
			SeeTilingOffset(Toggle): True
			TilingOffset(Vec): 1,1,0,0
			CustomFallback(Text): "_Normal_Map_GLTexture"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Smoothness"
			Number(Float): 0.8795926
			Range0(Float): 0
			Range1(Float): 0.986
			CustomFallback(Text): "_Smoothness"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Light Size"
			Number(Float): 0.5951468
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Light_Size"
		End Shader Input


		Begin Shader Input
			Type(Text): "Image"
			VisName(Text): "Metalness 2 - Texture"
			ImageDefault(Float): 0
			Image(Text): "ba42f659d4accc4468af49c1fd780e64"
			NormalMap(Float): 0
			DefaultTexture(Text): "White"
			SeeTilingOffset(Toggle): True
			TilingOffset(Vec): 1,1,0,0
			CustomFallback(Text): "_Metalness_2_GLTexture"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Fade Distance"
			Number(Float): 1
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Fade_Distance"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Transparency - Fill"
			Number(Float): 0.4077778
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Transparency_GLFill"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Metalness - Number"
			Number(Float): 0
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Metalness_GLNumber"
		End Shader Input


		Begin Shader Input
			Type(Text): "Color"
			VisName(Text): "Emission - Color A"
			Color(Vec): 1,1,1,1
			CustomFallback(Text): "_Emission_GLColor_A"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Albedo 2 Copy - Mix Amount"
			Number(Float): 1
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Albedo_2_Copy_GLMix_Amount"
		End Shader Input


		Begin Shader Input
			Type(Text): "Float"
			VisName(Text): "Emission - Power"
			Number(Float): 2
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Emission_GLPower"
		End Shader Input


		Begin Shader Input
			Type(Text): "Float"
			VisName(Text): "Emission - Multiply"
			Number(Float): 0.6
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Emission_GLMultiply"
		End Shader Input


		Begin Shader Input
			Type(Text): "Range"
			VisName(Text): "Normal Map - Height"
			Number(Float): 3
			Range0(Float): 0
			Range1(Float): 3
			CustomFallback(Text): "_Normal_Map_GLHeight"
		End Shader Input


		Begin Shader Input
			Type(Text): "Float"
			VisName(Text): "Emission - Multiply 2"
			Number(Float): 3.8
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Emission_GLMultiply_2"
		End Shader Input


		Begin Shader Input
			Type(Text): "Float"
			VisName(Text): "Metalness 2 - Power"
			Number(Float): 4.77
			Range0(Float): 0
			Range1(Float): 1
			CustomFallback(Text): "_Metalness_2_GLPower"
		End Shader Input

		ShaderName(Text): "sandwich_rim"
		Tech Lod(Float): 200
		Fallback(Type): Diffuse - {TypeID = 0}
		CustomFallback(Text): "\qLegacy Shaders/Diffuse\q"
		Queue(Type): Auto - {TypeID = 0}
		Custom Queue(Float): 2000
		QueueAuto(Toggle): True
		Replacement(Type): Auto - {TypeID = 0}
		ReplacementAuto(Toggle): True
		Tech Shader Target(Float): 3
		Exclude DX9(Toggle): False

		Begin Masks

		End Masks

		Begin Shader Pass
			Name(Text): "Pass 1"
			Visible(Toggle): True

			Begin Shader Ingredient

				Type(Text): "ShaderSurfaceTangentNormals"
				User Name(Text): "Tangent Normals"
				Use Custom Lighting(Toggle): False
				Use Custom Ambient(Toggle): False
				Alpha Blend Mode(Type): Blend - {TypeID = 0}
				Mix Amount(Float): 1
				Mix Type(Type): Mix - {TypeID = 0}
				Use Alpha(Toggle): True
				ShouldLink(Toggle): False
				Normalize(Toggle): True

				Begin Shader Layer List

					LayerListUniqueName(Text): "Normals"
					LayerListName(Text): "Normals"
					Is Mask(Toggle): False
					EndTag(Text): "rgb"

					Begin Shader Layer
						Layer Name(Text): "Normal Map"
						Layer Type(ObjectArray): SLTTexture - {ObjectID = 1}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Texture(Texture): 2acb00935be0b864088c61e647c2ac60 - {Input = 1}
						Color(Vec): 0,0,1,1
						Begin Shader Effect
							TypeS(Text): "SSENormalMap"
							IsVisible(Toggle): True
							UseAlpha(Float): 0
							Size(Float): 0.001666666
							Height(Float): 3 - {Input = 12}
							Channel(Type): A - {TypeID = 3}
							Normalize(Toggle): True
						End Shader Effect

					End Shader Layer

				End Shader Layer List

			End Shader Ingredient


			Begin Shader Ingredient

				Type(Text): "ShaderSurfaceDiffuse"
				User Name(Text): "Diffuse"
				Use Custom Lighting(Toggle): False
				Use Custom Ambient(Toggle): False
				Alpha Blend Mode(Type): Blend - {TypeID = 0}
				Mix Amount(Float): 1
				Mix Type(Type): Mix - {TypeID = 0}
				Use Alpha(Toggle): True
				ShouldLink(Toggle): True
				Lighting Type(Type): Unity Standard - {TypeID = 1}
				Roughness Or Smoothness(Type): Smoothness - {TypeID = 0}
				Smoothness(Float): 0.8795926 - {Input = 2}
				Roughness(Float): 0.7
				Light Size(Float): 0.5951468 - {Input = 3}
				Wrap Amount(Float): 0.7
				Wrap Color(Vec): 0.4,0.2,0.2,1
				PBR Quality(Type): Auto - {TypeID = 0}
				Disable Normals(Float): 0
				Use Ambient(Toggle): True
				Use Tangents(Toggle): True

				Begin Shader Layer List

					LayerListUniqueName(Text): "Albedo"
					LayerListName(Text): "Albedo"
					Is Mask(Toggle): False
					EndTag(Text): "rgba"

					Begin Shader Layer
						Layer Name(Text): "Albedo 2"
						Layer Type(ObjectArray): SLTTexture - {ObjectID = 1}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -1}
						Texture(Texture): bbd4c6882c708a14785e156f898327b3 - {Input = 0}
						Color(Vec): 0.5,0.8823529,1,1
					End Shader Layer

				End Shader Layer List

			End Shader Ingredient


			Begin Shader Ingredient

				Type(Text): "ShaderSurfaceUnlit"
				User Name(Text): "Unlit"
				Use Custom Lighting(Toggle): False
				Use Custom Ambient(Toggle): False
				Alpha Blend Mode(Type): Blend - {TypeID = 0}
				Mix Amount(Float): 1
				Mix Type(Type): Add - {TypeID = 1}
				Use Alpha(Toggle): True
				ShouldLink(Toggle): False
				RenderForEachLight(Toggle): False

				Begin Shader Layer List

					LayerListUniqueName(Text): "Surface"
					LayerListName(Text): "Unlit"
					Is Mask(Toggle): False
					EndTag(Text): "rgba"

					Begin Shader Layer
						Layer Name(Text): "Unlit2"
						Layer Type(ObjectArray): SLTColor - {ObjectID = 0}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Color(Vec): 0,0,0,1
					End Shader Layer

					Begin Shader Layer
						Layer Name(Text): "Albedo 2 Copy"
						Layer Type(ObjectArray): SLTTexture - {ObjectID = 1}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1 - {Input = 9}
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -1}
						Texture(Texture): bbd4c6882c708a14785e156f898327b3 - {Input = 0}
						Color(Vec): 0.5,0.8823529,1,1
					End Shader Layer

				End Shader Layer List

			End Shader Ingredient


			Begin Shader Ingredient

				Type(Text): "ShaderSurfaceSpecular"
				User Name(Text): "Specular"
				Use Custom Lighting(Toggle): False
				Use Custom Ambient(Toggle): False
				Alpha Blend Mode(Type): Blend - {TypeID = 0}
				Mix Amount(Float): 1
				Mix Type(Type): Mix - {TypeID = 0}
				Use Alpha(Toggle): False
				ShouldLink(Toggle): False
				Specular Type(Type): Unity Standard - {TypeID = 1}
				Roughness Or Smoothness(Type): Smoothness - {TypeID = 0}
				Smoothness(Float): 0.8795926 - {Input = 2}
				Roughness(Float): 0.7
				Light Size(Float): 0.5951468 - {Input = 3}
				Spec Energy Conserve(Toggle): True
				Spec Offset(Float): 0
				PBR Quality(Type): Auto - {TypeID = 0}
				PBR Model(Type): Metal - {TypeID = 1}
				Use Tangents(Toggle): True
				Use Ambient(Toggle): True
				Use Roughness Darkening(Toggle): True
				Use Fresnel(Toggle): True

				Begin Shader Layer List

					LayerListUniqueName(Text): "Specular"
					LayerListName(Text): "Metalness"
					Is Mask(Toggle): False
					EndTag(Text): "a"

					Begin Shader Layer
						Layer Name(Text): "Metalness 2"
						Layer Type(ObjectArray): SLTTexture - {ObjectID = 1}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): True
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Add - {TypeID = 1}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Texture(Texture): ba42f659d4accc4468af49c1fd780e64 - {Input = 4}
						Color(Vec): 0.5,0.8823529,1,1
						Number(Float): 0.5
						Begin Shader Effect
							TypeS(Text): "SSEMathPow"
							IsVisible(Toggle): True
							UseAlpha(Float): 0
							Power(Float): 4.77 - {Input = 14}
						End Shader Effect

					End Shader Layer

					Begin Shader Layer
						Layer Name(Text): "Metalness"
						Layer Type(ObjectArray): SLTNumber - {ObjectID = 7}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Subtract - {TypeID = 2}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Number(Float): 0 - {Input = 7}
						Color(Vec): 0.5,0.8823529,1,1
					End Shader Layer

				End Shader Layer List

			End Shader Ingredient


			Begin Shader Ingredient

				Type(Text): "ShaderSurfaceEmission"
				User Name(Text): "Emission"
				Use Custom Lighting(Toggle): False
				Use Custom Ambient(Toggle): False
				Alpha Blend Mode(Type): Blend - {TypeID = 0}
				Mix Amount(Float): 1
				Mix Type(Type): Add - {TypeID = 1}
				Use Alpha(Toggle): True
				ShouldLink(Toggle): False

				Begin Shader Layer List

					LayerListUniqueName(Text): "Emission"
					LayerListName(Text): "Emission"
					Is Mask(Toggle): False
					EndTag(Text): "rgba"

					Begin Shader Layer
						Layer Name(Text): "Emission2"
						Layer Type(ObjectArray): SLTColor - {ObjectID = 0}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Color(Vec): 0,0,0,1
					End Shader Layer

					Begin Shader Layer
						Layer Name(Text): "Emission"
						Layer Type(ObjectArray): SLTTexture - {ObjectID = 1}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Add - {TypeID = 1}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Color(Vec): 0.5,0.8823529,1,1
						Texture(Texture): f4b508660c8551146be2c17bea46d1ae
						Begin Shader Effect
							TypeS(Text): "SSEMathMul"
							IsVisible(Toggle): True
							UseAlpha(Float): 0
							Multiply(Float): 3.8 - {Input = 13}
						End Shader Effect

					End Shader Layer

					Begin Shader Layer
						Layer Name(Text): "Emission 2"
						Layer Type(ObjectArray): SLTGradient - {ObjectID = 9}
						UV Map(Type): Generate - {TypeID = 1}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Rim - {TypeID = 2}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Add - {TypeID = 1}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Color(Vec): 1,1,1,1 - {Input = 8}
						Color 2(Vec): 0,0,0,0
						Cheap Gamma Correct(Toggle): True
						Linearize(Toggle): True
						Scale(Float): 15
						Weird Noise Dimensions(Type): SSN/A - {TypeID = 1}
						Smoother(Toggle): True
						Gamma Correct(Toggle): True
						Cubemap(Cubemap): 
						HDR Support(Toggle): True
						Begin Shader Effect
							TypeS(Text): "SSEMathPow"
							IsVisible(Toggle): True
							UseAlpha(Float): 0
							Power(Float): 2 - {Input = 10}
						End Shader Effect

						Begin Shader Effect
							TypeS(Text): "SSEMathMul"
							IsVisible(Toggle): True
							UseAlpha(Float): 0
							Multiply(Float): 0.6 - {Input = 11}
						End Shader Effect

					End Shader Layer

				End Shader Layer List

			End Shader Ingredient


			Begin Shader Ingredient

				Type(Text): "ShaderSurfaceTransparency"
				User Name(Text): "Set Alpha Channel"
				Use Custom Lighting(Toggle): False
				Use Custom Ambient(Toggle): False
				Alpha Blend Mode(Type): Replace - {TypeID = 3}
				Mix Amount(Float): 1
				Mix Type(Type): Mix - {TypeID = 0}
				Use Alpha(Toggle): True
				ShouldLink(Toggle): False

				Begin Shader Layer List

					LayerListUniqueName(Text): "Transparency"
					LayerListName(Text): "Transparency"
					Is Mask(Toggle): False
					EndTag(Text): "a"

					Begin Shader Layer
						Layer Name(Text): "Transparency"
						Layer Type(ObjectArray): SLTRandomNoise - {ObjectID = 17}
						UV Map(Type): View - {TypeID = 5}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): World - {TypeID = 0}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Pixel Screen Position - {TypeID = 5}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): True
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -1}
						Noise Dimensions(Type): 2D - {TypeID = 0}
						Gamma Correct(Toggle): True
						Color(Vec): 1,1,1,1
						Number(Float): 0.5
						Texture(Texture): 92d0402287dd65c419a81b9a1895e96c
						Jitter(Float): 0.1
						MinSize(Float): 0
						MaxSize(Float): 1
						Square(Toggle): False
						Weird Noise Dimensions(Type): SSN/A - {TypeID = 1}
						Smoother(Toggle): True
						Swirl(Float): 1
						Image Based(Toggle): False
						RGBMask(ObjectArray): SSNone - {ObjectID = -1}
						ColorOrFloat(Type): SSN/A - {TypeID = 0}
						FillerInput1(Toggle): False
						FillerInput2(Toggle): False
						FillerInput3(Toggle): False
						Linearize(Toggle): True
						Scale(Float): 15
						Data(Type): SSN/A - {TypeID = 2}
						Fill(Float): 0.4077778 - {Input = 6}
						Edge(Float): 0.4077778 - {Input = 6}
					End Shader Layer

					Begin Shader Layer
						Layer Name(Text): "Transparency6"
						Layer Type(ObjectArray): SLTNumber - {ObjectID = 7}
						UV Map(Type): UV Map - {TypeID = 0}
						Map Local(Toggle): False
						Map Space(Type): World - {TypeID = 0}
						Map Generate Space(Type): Object - {TypeID = 1}
						Map Inverted(Toggle): True
						Map UV Index(Float): 1
						Map Direction(Type): Normal - {TypeID = 0}
						Map Screen Space(Type): Screen Position - {TypeID = 4}
						Use Fadeout(Toggle): False
						Fadeout Limit Min(Float): 0
						Fadeout Limit Max(Float): 10
						Fadeout Start(Float): 3
						Fadeout End(Float): 5
						Use Alpha(Toggle): False
						Alpha Blend Mode(Type): Blend - {TypeID = 0}
						Mix Amount(Float): 1
						Mix Type(Type): Mix - {TypeID = 0}
						Stencil(ObjectArray): SSNone - {ObjectID = -2}
						Number(Float): 0.5294118
						Color(Vec): 0.5,0.8823529,1,1
						Begin Shader Effect
							TypeS(Text): "SSEDistanceFade"
							IsVisible(Toggle): True
							UseAlpha(Float): 2
							Fadeout Limit Min(Float): 0
							Fadeout Limit Max(Float): 10
							Fadeout Start(Float): 0
							Fadeout End(Float): 1.92
							Fadeout Color(Vec): 0,0,0,0
							Fadeout Invert(Toggle): False
						End Shader Effect

					End Shader Layer

				End Shader Layer List

			End Shader Ingredient

			Geometry Ingredients

			Begin Shader Ingredient

				Type(Text): "ShaderGeometryModifierMisc"
				User Name(Text): "Misc Settings"
				Mix Amount(Float): 1
				ShouldLink(Toggle): False
				ZWriteMode(Type): Auto - {TypeID = 0}
				ZTestMode(Type): Auto - {TypeID = 0}
				CullMode(Type): Back Faces - {TypeID = 0}
				ShaderModel(Type): Auto - {TypeID = 0}
				Use Fog(Toggle): True
				Use Lightmaps(Toggle): True
				Use Forward Full Shadows(Toggle): True
				Use Forward Vertex Lights(Toggle): True
				Use Shadows(Toggle): True
				Generate Forward Base Pass(Toggle): True
				Generate Forward Add Pass(Toggle): True
				Generate Deferred Pass(Toggle): True
				Generate Shadow Caster Pass(Toggle): True

			End Shader Ingredient


			Begin Shader Ingredient

				Type(Text): "ShaderGeometryModifierTransparency"
				User Name(Text): "Transparency"
				Mix Amount(Float): 1
				ShouldLink(Toggle): False
				Transparency Type(Type): Cutout - {TypeID = 0}
				Transparency ZWrite(Type): On - {TypeID = 1}
				Shadow Type Fade(Type): Full - {TypeID = 0}
				Cutout Amount(Float): 0.5238096
				Transparency(Float): 1 - {Input = 5}
				Blend Mode(Type): Mix - {TypeID = 0}

			End Shader Ingredient

		End Shader Pass

	End Shader Base
End Shader Sandwich Shader
*/
