using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResourceEfficienyBar : MonoBehaviour
{

    public Image image;
    public Text TextField;

    public void SetEfficiency( float normalizedAmount )
    {
        image.fillAmount = normalizedAmount;
    }
}
