using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public struct MaterialRendererPair
{
    public Material material;
    public MeshRenderer renderer;

    public MaterialRendererPair( Material material, MeshRenderer renderer )
    {
        this.material = material;
        this.renderer = renderer;
    }
}

public class UnCompletedBuilding : Interactable
{

    public Structure affectedStructure;
    public float buildProgress = 0f;

    public List<MaterialRendererPair> Originals;
    // Use this for initialization
    void Start()
    {
        Originals = new List<MaterialRendererPair>();
        base.type = InteractionType.Buildable;
        affectedStructure = GetComponent<Structure>();
        buildProgress = 0f;
        SetAllBehaviours( false );
    }

    void SetAllBehaviours( bool value )
    {
        foreach ( Behaviour b in GetComponentsInChildren<Behaviour>() )
        {
            if ( b != this && !( b is NavMeshObstacle ) )
            {
                b.enabled = value;
            }
        }

        if ( value )
        {
            foreach ( MaterialRendererPair t in Originals )
            {
                t.renderer.sharedMaterial = t.material;
            }
        }
        else
        {
            foreach ( MeshRenderer r in GetComponentsInChildren<MeshRenderer>() )
            {
                Originals.Add( new MaterialRendererPair( r.sharedMaterial, r ) );
                r.material = InteractionController.Instance.InCompleteMaterial;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Interact()
    {
        buildProgress += .1f;

        if ( buildProgress >= 1f )
        {
            SetAllBehaviours( true );
            buildProgress = 1f;
            this.enabled = false;
        }
    }
}
