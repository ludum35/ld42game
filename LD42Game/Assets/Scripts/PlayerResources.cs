using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResources : Singleton<PlayerResources>
{
    public List<Resource> resources;
    public List<Resource> ResourceBoxValues;
    public int DefaultBoxValue = 0;
    public int ResourcePerHit = 2;
    public Resource GetResourceBoxValue( Resources c )
    {
        Resource baseValues = ResourceBoxValues.Find( ( x ) => x.type == c );
        
        if ( baseValues == null )
        {
            return new Resource( c, DefaultBoxValue );
        }
        return new Resource( baseValues.type, baseValues.Amount );
    }

    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool TakeFromStack( Resources res, int amount )
    {
        Resource r = getStack( res );
        if ( r.Amount >= amount )
        {
            r.Amount -= amount;
            return true;
        }
        return false;
    }

    public void AddToStack( Resources res, int amount )
    {
        if ( amount < 0 )
        {
            return;
        }

        Resource r = getStack( res );
        r.Amount += amount;
    }

    public Resource getStack( Resources res )
    {
        Resource resource = resources.Find( ( x ) => x.type == res );
        if ( resource == null )
        {
            Debug.Log( "no resource in list?" );
        }
        return resource;
    }
}
