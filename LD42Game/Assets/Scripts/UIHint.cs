using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class UIHint : Singleton<UIHint>
{
    public RectTransform hintPanel;
    public TextMeshProUGUI hintText;

    public float hintStartTime = 0f;
    public float hintCloseTime = 0f;
    private bool isHintShowing;

    public void DisplayHint( string text, float time = .2f )
    {
        isHintShowing = true;
        hintStartTime = Time.time;
        hintPanel.DOScaleY( 1f, .4f );
        hintText.text = text;
        hintCloseTime = hintStartTime + time;
    }

    private void CloseHint()
    {
        isHintShowing = false;
        hintPanel.DOScaleY( 0f, .4f );
    }
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        DisplayHint( "test hint" );
    }

    // Update is called once per frame
    void Update()
    {
        if ( isHintShowing )
        {
            if ( Time.time > hintCloseTime )
            {
                CloseHint();
            }
        }
    }
}
