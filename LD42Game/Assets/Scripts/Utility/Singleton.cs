using System;
using UnityEngine;

[Serializable]
public class Singleton<T> : SingletonBase where T : class
{


    public override void Start()
    {
        base.Start();
        TryCreateInstance();
    }

    public void TryCreateInstance()
    {
        if ( _instance != null )
        {
            bool hasNoInstance = true;
            try
            {
                if ( backing.gameObject != null )
                {
                    hasNoInstance = false;
                }
            }
            catch ( Exception e )
            {
                Debug.Log( "ya gone " + e.Message.ToString() );
            }
            finally
            {
                if ( hasNoInstance )
                {
                    backing = this;
                    _instance = this as T;
                    Debug.Log( "NO BACKING MONOBEHAVIOUR FOR " + typeof( T ).ToString() + " allowing new instance" );
                }
                else
                {
                    Debug.Log( "Instance for " + typeof( T ).ToString() + " already exists" );
                }
            }
        }
        backing = this;
        _instance = this as T;
    }

    private static T _instance;
    private static MonoBehaviour backing;

    public override void Initialize()
    {
        base.Initialize();
        TryCreateInstance();
    }

    public static T Instance
    {
        get
        {
            return _instance;
        }
    }
}

public abstract class SingletonBase : MonoBehaviour
{

    public bool Initialized = false;

    public virtual void Start()
    {
    }

    public virtual void Initialize()
    {
        Initialized = true;
    }
}