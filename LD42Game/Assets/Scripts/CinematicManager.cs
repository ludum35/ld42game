using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(500)]
public class CinematicManager : Singleton<CinematicManager> {

	// Use this for initialization
	public override void Start () {
        base.Start();
        if ( Player.Instance != null )
        {
            Player.Instance.enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
