using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSoundPicker : MonoBehaviour
{

    public List<AudioClip> effects;
    public float alternatePitch = .03f;
    public void Start()
    {
        PlaySound();
    }
    public void PlaySound()
    {
        if ( effects == null )
        {
            return;
        }

        if (alternatePitch > 0f)
        {
            GetComponent<AudioSource>().pitch += Random.Range( -alternatePitch, alternatePitch );
        }

        int rand = Random.Range( 0, effects.Count );

        GetComponent<AudioSource>().PlayOneShot( effects[rand] );
    }
}
