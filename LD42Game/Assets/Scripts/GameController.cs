using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

public enum GameState
{
    Normal,
    Defeat,
    Victory
}

[RequireComponent( typeof( LevelData ) )]
[DefaultExecutionOrder( -50 )]
public class GameController : MonoBehaviour
{
    public Vector3 CellSize = new Vector3( 16.0f, 4.0f, 16.0f );
    public float BorderSize = 2.0f;
    public float NPCOxygenPerS = 0.01f;
    public float NPCWaterPerS = 0.05f;
    public float NPCFoodPerS = 1.0f / 60.0f; // Eat once every 30 seconds
    public bool Cinematic = false;
    public int CinematicSeed = 51962343;

    private int NumStartingPillars;
    [SerializeField]
    internal float FoodMaximum;
    [SerializeField]
    internal float WaterMaximum;

    public float Water { get; set; }

    public float OxygenPercentage { get; set; }

    public float Food { get; set; }

    public LevelData LevelData { get; private set; }

    public static GameController Instance { get; private set; }

    public int NumNPCs { get; internal set; }

    public float ResourceShortage { get; private set; }

    public int NumLivePillars { get; set; }

    public GameState State { get; set; }

    public float LastOxygenDelta { get; set; }

    public float LastWaterDelta { get; set; }

    public float LastFoodDelta { get; set; }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        Assert.IsNull( Instance, "There can only be one active GameController" );
        Instance = this;

        LevelData = GetComponent<LevelData>();

        Vector2i center = LevelData.Size / 2;

        List<Vector2i> slumOptions = new List<Vector2i>( Vector2i.Neighbors );

        for( int i = 0; i < 3; ++i )
        {
            Vector2i p = slumOptions.Random();
            slumOptions.Remove( p );
            Structure slum = LevelData.CreateBlueprint( StructureType.Slum, center + p );
            slum.GetComponent<Slum>().StartingPopulation = 2 + i * 2;
        }


        Structure hack = LevelData.CreateBlueprint( StructureType.ReactorCore, center );

        LevelData.CreateBlueprint( StructureType.Engine, new Vector2i( LevelData.Size.x / 2, LevelData.Size.y - 1 ), 0.0f );

        const int pillarStride = 3;


        List<Vector2i> pillars = new List<Vector2i>();

        for( int x = 2; x < LevelData.Size.x; x += pillarStride )
        {
            for( int y = 2; y < LevelData.Size.y; y += pillarStride )
            {
                Vector2i p = new Vector2i( x, y );
                if( LevelData.IsEmpty( p ) )
                {
                    NumLivePillars++;
                    NumStartingPillars++;
                    LevelData.CreateBlueprint( StructureType.Pillar, p );

                    pillars.Add( p );
                }
            }
        }

        if( Cinematic )
        {
            LevelData.DoNotUseThis( center - Vector2i.one, hack );
            UnityEngine.Random.InitState( CinematicSeed );
        }

        int rotation;

        for( int i = 0; i < 3; ++i )
        {
            Vector2i p = pillars.Random();
            pillars.Remove( p );
            rotation = UnityEngine.Random.Range( 0, 4 );

            float angle = 0.0f;
            switch( rotation )
            {
                case 0:
                    angle = 270.0f;
                    break;
                case 1:
                    angle = 0.0f;
                    break;
                case 2:
                    angle = 180.0f;
                    break;
                case 3:
                    angle = 90.0f;
                    break;
            }

            LevelData.CreateBlueprint( StructureType.InfiniteResourceMetal + i, p + Vector2i.Neighbors[rotation], angle );
        }


        for( int i = 0; i < 4; ++i )
        {
            Vector2i r = new Vector2i( UnityEngine.Random.Range( 0, LevelData.Size.x ), UnityEngine.Random.Range( 0, LevelData.Size.y ) );
            if( LevelData.IsEmpty( r ) )
                LevelData.CreateBlueprint( StructureType.ResourcesSpot, r );
        }


        Water = 100;
        OxygenPercentage = 100;
        Food = 75;
    }

    void FixedUpdate()
    {

        LastOxygenDelta = NPCOxygenPerS * NumNPCs;
        LastWaterDelta = NPCWaterPerS * NumNPCs;
        LastFoodDelta = NPCFoodPerS * NumNPCs;

        OxygenPercentage = OxygenPercentage - LastOxygenDelta * Time.deltaTime;
        Water = Water - LastWaterDelta * Time.deltaTime;
        Food = Food - LastFoodDelta * Time.deltaTime;

        ResourceShortage = Mathf.Min( OxygenPercentage, 0 ) + Mathf.Min( Water, 0 ) + Mathf.Min( Food, 0 );

        OxygenPercentage = Mathf.Max( 0, OxygenPercentage );
        Water = Mathf.Max( 0, Water );
        Food = Mathf.Max( 0, Food );

        if( NumLivePillars < NumStartingPillars / 2 )
        {
            State = GameState.Defeat;
        }
    }

    public Vector2i WorldToTile( Vector3 worldPosition )
    {
        Vector3 unitPos = worldPosition.Div( CellSize );

        return new Vector2i( Mathf.FloorToInt( unitPos.x ), Mathf.FloorToInt( unitPos.z ) );
    }

    public Vector3 TileToWorldCentered( Vector2i cell )
    {
        Vector3 cellPos = new Vector3( cell.x, 0, cell.y );

        cellPos = Vector3.Scale( cellPos, CellSize );
        cellPos += new Vector3( CellSize.x, 0, CellSize.z ) / 2.0f;

        return cellPos;
    }

    public Structure AttemptPlace( StructureType placing, Vector2i tile )
    {
        if( !LevelData.IsEmpty( tile ) && placing != StructureType.LivingSpace )
            return null;

        switch( placing )
        {
            case StructureType.WaterFilter:
                return LevelData.CreateBlueprint( StructureType.WaterFilter, tile );
            case StructureType.AirFilter:
                return LevelData.CreateBlueprint( StructureType.AirFilter, tile );
            case StructureType.Farm:
                return LevelData.CreateBlueprint( StructureType.Farm, tile );
            case StructureType.LivingSpace:
                Structure current = LevelData.GetStructure( tile );

                Slum slum = current.GetComponent<Slum>();
                if( slum != null )
                {
                    NPCAI[] npcs = current.GetStructureNPCs();

                    current.transform.Find( "NPCContainer" ).DetachChildren();

                    LevelData.RemoveStructure( tile );
                    Destroy( slum.gameObject );
                    Structure newStructure = LevelData.CreateBlueprint( StructureType.LivingSpace, tile );
                    newStructure.GetComponent<LivingSpace>().NumLayers = 1;

                    foreach( NPCAI npc in npcs )
                    {
                        npc.ReParent( newStructure );
                    }
                    return newStructure;
                }
                else
                {

                    LivingSpace ls = current.GetComponent<LivingSpace>();
                    GameObject newLayer = Instantiate( LevelData.StructurePrefabs.LivingSpaces.Random().gameObject, ls.transform );
                    Destroy( newLayer.GetComponent<Repairable>() );
                    Destroy( newLayer.GetComponent<AudioSource>() );
                    Destroy( newLayer.GetComponent<Structure>() );
                    Destroy( newLayer.GetComponent<LivingSpace>().Console.gameObject );
                    Destroy( newLayer.GetComponent<LivingSpace>() );
                    Destroy( newLayer.GetComponentInChildren<NavMeshObstacle>() );
                    newLayer.transform.localPosition = new Vector3( 0, 3.75f * ls.NumLayers, 0.0f );
                    newLayer.transform.rotation = Quaternion.Euler( 0.0f, UnityEngine.Random.Range( 0, 4 ) * 90.0f, 0.0f );
                    ls.NumLayers++;
                    return null;
                }

        }
        return null;
    }
}
