using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StructureConsole : MonoBehaviour
{
    public TextMeshProUGUI ContentTextMesh;


    protected Canvas canvas;
    protected CanvasGroup canvasGroup;
    protected bool isDisabling;

    public string Content { get; set; }

    private void Start()
    {
        InvokeRepeating( "UpdateContent", 0, 1.0f );

        canvas = GetComponentInChildren<Canvas>();
        canvas.enabled = false;

        canvasGroup = GetComponentInChildren<CanvasGroup>();
    }

    public void UpdateContent()
    {
        if (Player.Instance == null)
        {
            return;
        }
        if( Vector3.Distance( transform.position, Player.Instance.transform.position ) < 15.0f )
        {
            if( !canvas.enabled )
            {
                StartCoroutine( TurnOn() );
            }
        }
        else
        {
            if( canvas.enabled && !isDisabling )
            {
                isDisabling = true;
                canvasGroup.DOFade( 0.0f, 0.5f ).OnComplete( () => {
                    canvas.enabled = false;
                    isDisabling = false;
                } );
            }
        }

        ContentTextMesh.text = Content;
    }

    private IEnumerator TurnOn()
    {
        canvas.enabled = true;

        float tStart = Time.time;
        const float duration = 1.0f;

        while( Time.time - tStart < duration )
        {
            float s = 1.0f - ( ( Time.time - tStart ) / duration );
            float t = Random.Range( 0.2f, 0.35f ) * s;

            canvasGroup.alpha = 1.0f - s;
            canvasGroup.DOFade( Mathf.Clamp01( s + 0.5f ), t );
            yield return new WaitForSeconds( t );
        }

        canvasGroup.alpha = 1.0f;
    }
}
