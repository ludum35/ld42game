
using UnityEngine;

public class FollowTarget : Targetable
{
    public Vector3 offset = new Vector3( 0f, 7.5f, 0f );


    private void Update()
    {
        transform.position = Target.position + offset;
    }
}