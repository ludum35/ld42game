﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[DefaultExecutionOrder( 100 )]
public class MainMenuController : MonoBehaviour
{
    public Text pulseText;

    private List<Slum> newSlums = new List<Slum>();

    void Start()
    {
        LevelData levelData = GetComponent<LevelData>();
        Slum slum = FindObjectOfType<Slum>();
        
        for( int i = 0; i < 16; ++i )
        {
            Structure structure = slum.GetComponent<Structure>();

            Vector2i random;
            if( structure.FindNewExpansionTile( out random ) )
            {
                if( !levelData.IsEmpty( random ) )
                    continue;

                Structure s = levelData.CreateBlueprint( StructureType.Slum, random );
                if( s != null )
                {
                    s.GetComponent<Slum>().StartingPopulation = UnityEngine.Random.Range( 2, 6 );
                    newSlums.Add( s.GetComponent<Slum>() );
                }
            }
        }

        StartCoroutine( DelayAttack() );
    
        pulseText.DOFade( 0.5f, 1.0f ).SetLoops( -1, LoopType.Yoyo );
    }

    void Update()
    {
        if( Input.anyKey )
        {
            SceneManager.LoadScene( "Game_Cinematic", LoadSceneMode.Single );
        }

    }

    IEnumerator DelayAttack()
    {
        yield return new WaitForFixedUpdate();

        foreach( Slum slum in newSlums )
        {
            slum.RiotShortcut( UnityEngine.Random.Range( 0.25f, 5.0f ) );

            NPCAI[] npcs = slum.GetComponent<Structure>().GetStructureNPCs();
            foreach( NPCAI npc in npcs )
            {
                npc.GetComponent<LODGroup>().ForceLOD( 0 );
            }
        }

        GameController.Instance.enabled = false;
    }
}
