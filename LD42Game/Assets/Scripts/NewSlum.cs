﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Structure ) )]
public class NewSlum : MonoBehaviour
{
    private Structure structure;
    private Coroutine constructCoroutine;

    private void Start()
    {
        structure = GetComponent<Structure>();
    }

    private void FixedUpdate()
    {
        if( constructCoroutine == null )
        {
            NPCAI[] npcs = structure.GetStructureNPCs();

            bool allArrived = true;
            foreach( NPCAI npc in npcs )
            {
                if( !npc.ArrivedAtStructure )
                {
                    allArrived = false;
                    break;
                }
            }

            if( allArrived )
            {
                constructCoroutine = StartCoroutine( ConstructCoroutine() );
            }
        }
    }

    public IEnumerator ConstructCoroutine()
    {
        yield return new WaitForSeconds( 1.0f );

        NPCAI[] npcs = structure.GetStructureNPCs();

        GameController controller = GameController.Instance;
        controller.LevelData.RemoveStructure( structure.TilePosition );
        Structure slum = controller.LevelData.CreateBlueprint( StructureType.Slum, structure.TilePosition );

        if( slum != null )
        {
            foreach( NPCAI npc in npcs )
            {
                slum.AddNPCAsChild( npc );
            }
        }

        Destroy( gameObject );
    }
}
