﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( 25 )]
public class LivingSpace : MonoBehaviour
{
    public float NPCSpawnTime = 10.0f;
    public StructureConsole Console;

    protected Structure structure;
    protected float spawnTimer;
    protected int numVirtualNPCs;


    public int NumLayers { get; set; }

    void Start()
    {
        structure = GetComponent<Structure>();

        structure.IsExpansionEnabled = false;

        NumLayers = 1;

        structure.EnableOverpopulationUpdate();


        if( Console != null )
            InvokeRepeating( "UpdateConsole", 0, 1.0f );
    }

    private void OnDestroy()
    {
        GameController.Instance.NumNPCs -= numVirtualNPCs;
    }

    void FixedUpdate()
    {
        spawnTimer += Time.deltaTime;

        if( spawnTimer > NPCSpawnTime )
        {
            spawnTimer -= NPCSpawnTime;

            SpawnNPC();
        }

        structure.TickAnger( numVirtualNPCs, structure.PopPerLayer * NumLayers, AngerMod() );
    }

    public float AngerMod()
    {
        const float LivingSpaceAngerMod = 0.75f;
        return Mathf.Pow( LivingSpaceAngerMod, NumLayers );
    }

    public void UpdateConsole()
    {
        Console.Content = string.Format( "People: {0}\nLayers: {1}\nAnger reduction: {2}%",
            structure.GetStructureNumNPCs(), 
            NumLayers, 
            ( ( 1.0f - AngerMod() ) * 100.0f ).ToString( "0.00" ) );
    }

    private void SpawnNPC()
    {
        int numPop = structure.GetStructureNumNPCs();

        if( numPop < structure.PopPerLayer )
        {
            structure.SpawnNPC( structure.PopPerLayer * NumLayers );
        }
        else if( numPop < structure.PopPerLayer * NumLayers )
        {
            numVirtualNPCs++;
            GameController.Instance.NumNPCs++;
        }
    }
}
