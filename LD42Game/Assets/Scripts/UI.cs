using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : Singleton<UI>
{

    public Transform HitTarget;
    public Text InteractionText;
    public string InteractionHittableText;
    public string InteractionPickableText;
    public string InteractionBuildableText;
    // Use this for initialization


    // Update is called once per frame
    void Update()
    {
        ShowInteractionText();
    }

    public void ShowInteractionText()
    {
        if( InteractionController.Instance != null )
        {
            Interactable interactable = InteractionController.Instance.Interactable;
            if( interactable != null )
            {
                InteractionText.enabled = true;
                if( interactable.type == Interactable.InteractionType.Hittable )
                {
                    ResourceInteractable p = interactable as ResourceInteractable;
                    string hint = InteractionHittableText;
                    if( p != null )
                    {
                        hint = string.Format(
                            "Left click to hit {0} remaining:{1:N1}",
                            System.Enum.GetName( typeof( Resources ), p.Resource.type ),
                            p.Resource.Amount );
                    }

                    UIHint.Instance.DisplayHint( hint );
                }
                else if( interactable.type == Interactable.InteractionType.Pickable )
                {
                    ResourceInteractable b = interactable as ResourceInteractable;
                    if( b != null )
                    {
                        UIHint.Instance.DisplayHint( string.Format( InteractionPickableText,
                            System.Enum.GetName( typeof( Resources ), b.Resource.type ),
                            b.Resource.Amount ) );
                    }
                }
                else if( interactable.type == Interactable.InteractionType.Buildable )
                {
                    if( interactable.GetComponent<UnCompletedBuilding>() )
                    {
                        if( interactable.GetComponent<UnCompletedBuilding>().buildProgress < 1f )
                        {
                            
                            UIHint.Instance.DisplayHint( string.Format( "Left click to build" ) );
                        }
                    }

                }
                else if( interactable.type == Interactable.InteractionType.Repairable )
                {
                    Structure structure = interactable.GetComponent<Structure>();

                    if( structure.Health < 100.0f )
                    {
                        UIHint.Instance.DisplayHint( string.Format( "{0:N1}/100 health. Left click to repair", structure.Health ) );
                    }
                }
                else if( interactable.type == Interactable.InteractionType.Engine )
                {
                    UIHint.Instance.DisplayHint( string.Format( "Leftclick to repair engine. Progress: {0}%", interactable.GetComponent<Engine>().GetCompletion() ) );
                }

                return;
            }
        }
        InteractionText.enabled = false;
    }
}
