using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : Singleton<InteractionController>
{

    public Material HighlightMat;
    public Material InCompleteMaterial;

    public Interactable Interactable;

    public RaycastHit lastInteractableHit;

    private Material OldMaterial;
    public float InteractionDistance = 10f;
    // Update is called once per frame
    void Update()
    {
        Interactable newInteractable = GetRayCastInteractable();

        if ( Interactable != newInteractable )
        {
            ResetInteractable();

            SetInteractable( newInteractable );
        }
    }

    private void SetInteractable( Interactable inter )
    {
        Interactable = inter;

        if ( inter == null )
        {
            return;
        }

        if ( !Interactable.GetComponent<MeshRenderer>() )
        {
            return;
        }

        OldMaterial = Interactable.GetComponent<MeshRenderer>().sharedMaterial;

        Interactable.GetComponent<MeshRenderer>().sharedMaterial = HighlightMat;
    }

    private void ResetInteractable()
    {
        if ( Interactable != null )
        {
            MeshRenderer r = Interactable.GetComponent<MeshRenderer>();
            if ( r == null )
            {
                return;
            }
            r.sharedMaterial = OldMaterial;
        }
    }


    private Interactable GetRayCastInteractable()
    {
        Camera cam = Camera.main;

        Ray r = new Ray( cam.transform.position, cam.transform.forward );

        RaycastHit rayCastHit = new RaycastHit();

        int interactableLayer = ~( 1 << LayerMask.NameToLayer( "Ignore Raycast" ) );

        Physics.Raycast( r, out rayCastHit, InteractionDistance, interactableLayer );

        lastInteractableHit = rayCastHit;

        if ( rayCastHit.transform != null )
        {
            if ( rayCastHit.transform.gameObject.tag == "Ignore Interaction" )
            {
                return null;
            }

            Interactable directInteraction = rayCastHit.transform.GetComponent<Interactable>();

            if ( directInteraction != null )
            {
                return directInteraction;
            }

            Interactable[] interactables = rayCastHit.transform.root.GetComponentsInChildren<Interactable>();

            foreach ( Interactable t in interactables )
            {
                if ( t.enabled )
                {
                    return t;
                }
            }
        }


        return null;
    }
}
