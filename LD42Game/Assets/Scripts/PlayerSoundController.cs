using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Player ) )]
public class PlayerSoundController : MonoBehaviour
{
    public List<AudioClip> FootSteps;

    private bool leftFootCycleDone;
    private bool rightFootCycleDone;

    public Vector3 LeftFootPos;
    public Vector3 RightFootPos;

    public Vector3 thisPos;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void OnAnimatorIK()
    {
        thisPos = transform.position;
        LeftFootPos = GetComponent<Animator>().GetIKPosition( AvatarIKGoal.LeftFoot );
        RightFootPos = GetComponent<Animator>().GetIKPosition( AvatarIKGoal.RightFoot );

        if ( LeftFootPos.y - transform.position.y < .2f )
        {
            if ( !leftFootCycleDone )
            {
                PlayFootStep();
                leftFootCycleDone = true;
            }
        }
        else
        {
            leftFootCycleDone = false;
        }

        if ( RightFootPos.y - transform.position.y < .2f )
        {
            if ( !rightFootCycleDone )
            {
                PlayFootStep();
                rightFootCycleDone = true;
            }
        }
        else
        {
            rightFootCycleDone = false;
        }
    }

    private void PlayFootStep()
    {
        if ( FootSteps == null )
        {
            return;
        }

        int rand = Random.Range( 0, FootSteps.Count );

        GetComponent<AudioSource>().PlayOneShot( FootSteps[rand] );
    }
}
