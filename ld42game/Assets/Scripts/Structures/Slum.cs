﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Structure ) )]
public class Slum : MonoBehaviour
{
    public float NPCSpawnTime = 10.0f;
    public StructureConsole Console;    


    private float spawnTimer;
    private Structure structure;


    public int StartingPopulation { get; set; }

    void Start()
    {
        structure = GetComponent<Structure>();
        
        for( int i = 0; i < StartingPopulation; ++i )
        {
            structure.SpawnNPC( structure.PopPerLayer );
        }

        structure.EnableOverpopulationUpdate();
        
        if( Console != null )
            InvokeRepeating( "UpdateConsole", 0, 1.0f );
    }


    private void Update()
    {
        if( Input.GetKey( KeyCode.F8 ))
        {
            RiotShortcut();
        }
    }

    public void RiotShortcut( float delay = 1.0f )
    {
        StartCoroutine( DebugAttackDelayed( delay ) );
    }

    void FixedUpdate()
    {
        spawnTimer += Time.deltaTime;

        if( spawnTimer > NPCSpawnTime )
        {
            spawnTimer -= NPCSpawnTime;

            structure.SpawnNPC( structure.PopPerLayer );
        }

        structure.TickAnger( 0, structure.PopPerLayer, 1.0f );
    }

    public void UpdateConsole()
    {
        Console.Content = string.Format( "People: {0}\n", structure.GetStructureNumNPCs() );
    }


    private IEnumerator DebugAttackDelayed( float delay )
    {
        yield return new WaitForSeconds( delay );

        NPCAI[] npcs = structure.GetStructureNPCs();
        foreach( NPCAI npc in npcs )
        {
            npc.Riot( structure.TilePosition + Vector2i.up );
        }

    }
}
