﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Structure ) )]
public class Repairable : Interactable
{
    private Structure structure;
    
    private void Start()
    {
        structure = GetComponent<Structure>();
    }

    public override void Interact()
    {
        if( structure.Health < 100.0f )
        {
            structure.Health = Mathf.Min( 100.0f, structure.Health + 5.0f );
        }
    }
}
