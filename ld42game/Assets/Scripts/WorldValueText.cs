﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WorldValueText : MonoBehaviour
{

    void Start()
    {
        transform.DOMove( transform.position + Vector3.up * 4.0f, 1.0f ).SetEase( Ease.InCirc );
        
        
        Tweener t = GetComponentInChildren<TextMeshProUGUI>().DOFade( 0.0f, 0.5f );
        t.SetDelay( 0.5f );
        t.OnComplete( () => {
            Destroy( gameObject );
        } );
    }

    private void Update()
    {
        transform.LookAt( Camera.main.transform, Vector3.up );
        transform.forward = -transform.forward;
    }
}
