using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct TimedMessage
{
    public float StartTime;
    public float MessageTime;
    public string Message;
}

public class TimedHintMessages : MonoBehaviour
{
    public float startTime;
    public float runTime;
    public bool enabled = true;
    public List<TimedMessage> timedMessages;
    public bool skip = false;
    private float endAt;
    // Use this for initialization
    void Start()
    {
        if ( timedMessages == null )
        {
            timedMessages = new List<TimedMessage>();
        }

        endAt = runTime;

        startTime = Time.time;

        Time.timeScale = 0.75f;
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKeyDown( KeyCode.P ) )
        {
            skip = true;
        }

        runTime = Time.time - startTime;

        if ( runTime > endAt || skip )
        {
            Time.timeScale = 1.0f;
            SceneManager.LoadScene( "Game", LoadSceneMode.Single );
        }
        else
        {
            if ( !enabled )
            {
                return;
            }
            for ( int i = 0; i < timedMessages.Count; i++ )
            {
                TimedMessage t = timedMessages[i];
                if ( t.StartTime < Time.time - startTime )
                {
                    UIHint.Instance.DisplayHint( t.Message, t.MessageTime );
                    timedMessages.RemoveAt( i );
                    i--;
                }
            }
        }
    }
}
