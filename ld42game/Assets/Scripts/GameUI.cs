using System.Collections;
using System.Collections.Generic;
using TMPro;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

[DefaultExecutionOrder( 50 )]
public class GameUI : MonoBehaviour
{
    public TextMeshProUGUI WaterLabel;
    public TextMeshProUGUI OxygenLabel;
    public TextMeshProUGUI FoodLabel;
    public TextMeshProUGUI PopLabel;

    public TextMeshProUGUI BuildingText;

    public Transform BuildPanel;
    public UIBuildingPanels BuildablesPanel;

    public GameObject DefeatLayer;
    public GameObject VictoryLayer;

    public Image BuildingNormalizedProgressBar;

    public Image OxygenFill;
    public Image WaterFill;
    public Image FoodFill;

    public TextMeshProUGUI OxygenRate;
    public TextMeshProUGUI WaterRate;
    public TextMeshProUGUI FoodRate;

    GameController controller;

    private UIBuildingPanels.UIBuildingPanel oldSelected;

    void Start()
    {
        controller = GameController.Instance;
    }

    public void UpdatePlayerBuilding()
    {
        if ( Player.Instance == null || Player.Instance.Placing < 0 )
        {
            BuildablesPanel.panelMask.GetComponent<Image>().DOFillAmount( 0.12f, .4f );
            return;
        }

        UIBuildingPanels.UIBuildingPanel currentSelected = BuildablesPanel.getPanel( Player.Instance.IntToStructureType( Player.Instance.Placing ) );

        if ( currentSelected == null )
        {
            if ( oldSelected != null )
            {
                oldSelected.panel.GetComponent<Image>().DOColor( new Color( 1f, 1f, 1f, 1f ), .4f );
                oldSelected = null;
            }
            BuildablesPanel.panelMask.GetComponent<Image>().DOFillAmount( 0.12f, .4f );

            return;
        }

        if ( oldSelected != null )
        {
            if ( oldSelected.type != currentSelected.type )
            {
                oldSelected.panel.GetComponent<Image>().DOColor( new Color( 1f, 1f, 1f, 1f ), .4f );
            }
        }

        oldSelected = currentSelected;

        float color = 1f - ( Mathf.Repeat( Time.time, .5f ) ) * 1.5f;

        oldSelected.panel.GetComponent<Image>().DOColor( new Color( color, color, color ), .1f );

        BuildablesPanel.panelMask.GetComponent<Image>().DOFillAmount( 1f, .4f );
    }

    public void UpdateBuildProgress()
    {
        if ( InteractionController.Instance != null && InteractionController.Instance.Interactable != null )
        {
            UnCompletedBuilding build = InteractionController.Instance.Interactable as UnCompletedBuilding;
            if ( build != null )
            {
                BuildPanel.GetComponent<RectTransform>().DOScaleY( 1f, .1f );
                BuildPanel.GetComponent<RectTransform>().DOScaleX( 1f, .4f );

                BuildingText.text = string.Format( "{0:N1}%", build.buildProgress * 100f );
                BuildingNormalizedProgressBar.DOFillAmount( build.buildProgress, 1f );
                return;
            }
        }

        BuildPanel.GetComponent<RectTransform>().DOScaleY( 0f, .1f );
        BuildPanel.GetComponent<RectTransform>().DOScaleX( 0f, .4f );

    }


    void FixedUpdate()
    {

        if ( controller == null )
        {
            return;
        }

        UpdateBuildProgress();

        UpdatePlayerBuilding();

        WaterFill.DOFillAmount( GameController.Instance.Water / GameController.Instance.WaterMaximum, 1f );
        WaterLabel.text = string.Format( "water: {0}L", controller.Water.ToString( "0" ) );

        OxygenFill.DOFillAmount( controller.OxygenPercentage / 100f, 1f );
        OxygenLabel.text = string.Format( "oxygen: {0}%", controller.OxygenPercentage.ToString( "0.00" ) );

        FoodFill.DOFillAmount( GameController.Instance.Food / GameController.Instance.FoodMaximum, 1f );
        FoodLabel.text = string.Format( "burgers: {0}", controller.Food.ToString( "0" ) );
        PopLabel.text = string.Format( "pop: {0}", controller.NumNPCs );


        float oxygenDelta = AirFilter.TotalProduction - GameController.Instance.LastOxygenDelta;
        float waterDelta = WaterFilter.TotalProduction - GameController.Instance.LastWaterDelta;
        float foodDelta = Farm.TotalProduction - GameController.Instance.LastFoodDelta;

        OxygenRate.text = ToLex( oxygenDelta, 0.5f, 1.0f, 2.5f );
        WaterRate.text = ToLex( waterDelta, 1.0f, 2.5f, 5.0f );
        FoodRate.text = ToLex( foodDelta, 0.1f, 0.2f, 0.5f );

        if ( controller.State == GameState.Defeat )
        {
            DefeatLayer.SetActive( true );
            Player.Instance.GetComponent<FirstPersonController>().GetMouseLook.SetCursorLock( false );
        }
        if ( controller.State == GameState.Victory )
        {
            VictoryLayer.SetActive( true );
            Player.Instance.GetComponent<FirstPersonController>().GetMouseLook.SetCursorLock( false );
        }


    }

    private string ToLex( float value, float b1, float b2, float b3 )
    {
        string c = value >= 0 ? "+" : "-";

        value = Mathf.Abs( value );

        if ( value > b3 )
            return c + c + c;
        if ( value > b2 )
            return c + c;
        return c;
    }
}
