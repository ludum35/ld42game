using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( 20 )]
[RequireComponent( typeof( Structure ) )]
public class Farm : MonoBehaviour
{
    public float ProductionSpeed;
    public float ProductionAmount;
    public StructureConsole Console;

    private float productionTimer;
    private Structure structure;

    private float lastProduction;


    public static float TotalProduction { get; set; }

    private void OnDestroy()
    {
        TotalProduction -= lastProduction;
    }

    void Start()
    {
        structure = GetComponent<Structure>();
        structure.EnableResourceEfficiencyUpdate();

        if( Console != null )
            InvokeRepeating( "UpdateConsole", 0, 1.0f );
    }

    void FixedUpdate()
    {
        GameController controller = GameController.Instance;

        TotalProduction -= lastProduction;
        lastProduction = GetProdcutionValue() / ProductionSpeed;
        TotalProduction += lastProduction;

        productionTimer += Time.deltaTime;
        if( productionTimer > ProductionSpeed )
        {
            productionTimer -= ProductionSpeed;

            controller.Food = Mathf.Min( controller.Food + GetProdcutionValue(), GameController.Instance.FoodMaximum );
        }
    }

    float GetProdcutionValue()
    {
        return ProductionAmount * structure.ResourceEfficiency;
    }

    void UpdateConsole()
    {
        float nextHavestIn = ProductionSpeed - productionTimer;
        Console.Content = string.Format( "production: {0} food per harvest\neffeciency: {1}%\nnext harvest: {2}s",
            GetProdcutionValue().ToString( "0" ),
            ( structure.ResourceEfficiency * 100.0f ).ToString( "0" ),
            nextHavestIn.ToString( "0" ) );
    }
}
