﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : Interactable
{
    public float TotalCompletion = 100.0f;
    public float CompletionPerInteract = 1.0f;
    public StructureConsole Console;

    public List<Resource> RepairCost;


    private Structure structure;
    private float repairT;

    public float Completion { get; set; }

    private void Start()
    {
        structure = GetComponent<Structure>();

        structure.Health = 1000.0f;
        Completion = 0.0f;

        UpdateConsole();
    }

    private void OnDestroy()
    {
        GameController.Instance.State = GameState.Defeat;
    }

    public override void Interact()
    {
        PlayerResources resources = PlayerResources.Instance;

        bool canAfford = true;
        foreach( Resource repair in RepairCost )
        {
            if( resources.getStack( repair.type ).Amount < repair.Amount )
            {
                canAfford = false;
                break;
            }
        }

        if( canAfford && repairT <= 0.0f )
        {
            foreach( Resource repair in RepairCost )
            {
                resources.TakeFromStack( repair.type, repair.Amount );
            }
                        
            Completion += CompletionPerInteract;

            repairT = 2.0f;

            UpdateConsole();

            if( Completion >= TotalCompletion )
            {
                GameController.Instance.State = GameState.Victory;
            }
        }
    }

    private void Update()
    {
        if( repairT > 0.0f )
        {
            repairT -= Time.deltaTime;
            UpdateConsole();
        }
    }

    public float GetCompletion()
    {
        return ( Completion / TotalCompletion ) * 100.0f;
    }

    public void UpdateConsole()
    {
        if( Console != null )
        {
            string content = string.Format( "Repair progress: {0}%\n", ( GetCompletion() ).ToString( "0.00" ) );

            if( repairT > 0.0f )
            {
                content += string.Format( "Repairing... {0}s", repairT.ToString( "0.00" ) );
                Console.Content = content;
                Console.UpdateContent();
            }
            else
            {
                foreach( Resource repair in RepairCost )
                {
                    content += string.Format( "{0} {1}\n", repair.Amount, System.Enum.GetName( typeof( Resources ), repair.type ) );
                }
                Console.Content = content;
            }

        }
    }
}
