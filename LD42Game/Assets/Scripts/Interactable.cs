using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    public enum InteractionType
    {
        Hittable,
        Pickable,
        Buildable,
        Repairable,
        Engine
    }

    public InteractionType type;


    public virtual void Interact()
    {

    }
}
