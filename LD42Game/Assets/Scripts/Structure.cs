﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StructureType
{
    Slum,
    WaterFilter,
    AirFilter,
    Farm,
    LivingSpace,
    NewSlum,
    ReactorCore,
    Pillar,
    ResourcesSpot,
    Engine,
    InfiniteResourceMetal,
    InfiniteResourceElectronics,
    InfiniteResourceConcrete
}

public class Structure : MonoBehaviour
{
    public int ResourceEfficiencyRadius = 2;
    public Sprite MiniMapSprite;
    public GameObject NPCPrefab;
    public int PopPerLayer = 8;
    public float OverpopulationAngerModifier = 0.1f;
    public float AngerPerS = 0.1f;
    public float AngerDecayS = 0.1f;
    public List<GameObject> DestroyFx;
    public List<GameObject> DamageIndicators;
    public List<GameObject> DamageCreateIndicators;

    private bool ResourceEfficiencyUpdateEnabled = false;
    private bool OverpopulationUpdateEnabled = false;
    private Transform npcContainer;
    private float overpopulationModifier = 1.0f;
    private float catharsis = 0.0f;
    private List<GameObject> activeDamageIndicators = new List<GameObject>();

    public bool IsExpansionEnabled { get; set; }
    
    public float ResourceEfficiency { get; private set; }

    public Vector2i TilePosition { get; private set; }

    public GameObject MinimapObject { get; private set; }

    public float Anger { get; private set; }

    public float Health { get; set; }

    public void InitializeStructure( Vector2i position )
    {
        this.TilePosition = position;

        ResourceEfficiency = 1.0f;
        IsExpansionEnabled = true;

        if( MiniMapSprite != null )
        {
            MinimapObject = new GameObject( "MiniMapSprite", new System.Type[] { typeof( SpriteRenderer ) } );
            MinimapObject.GetComponent<SpriteRenderer>().sprite = MiniMapSprite;
            MinimapObject.transform.SetParent( transform );
            MinimapObject.transform.localPosition = Vector3.zero;
            MinimapObject.transform.rotation = Quaternion.Euler( 90.0f, 90.0f, 0.0f );
            MinimapObject.transform.localScale = Vector3.one * 12.0f;
            MinimapObject.layer = LayerMask.NameToLayer( "MiniMap" );
        }
    }

    private void Start()
    {
        Health = 100.0f;
    }

    private void OnDestroy()
    {
        if( ResourceEfficiencyUpdateEnabled || OverpopulationUpdateEnabled )
            DisableOnNearBuildingListener();

        foreach( GameObject fx in activeDamageIndicators )
        {
            Destroy( fx );
        }
    }

    public int GetStructureNumNPCs()
    {
        if( npcContainer == null )
            return 0;
        return npcContainer.childCount;
    }

    public void FixedUpdate()
    {
        if( Health <= 0.0f )
        {

            foreach( GameObject fx in DestroyFx )
            {
                GameObject newFX = Instantiate( fx );
                newFX.transform.position = transform.position;
            }

            GameController.Instance.LevelData.RemoveStructure( TilePosition );
            Destroy( gameObject );
        }

        int numIndicators = 5 - Mathf.FloorToInt( Health / 20.0f );
        for( int i = 0; i < numIndicators; ++i )
        {
            if( activeDamageIndicators.Count < numIndicators )
            {
                Ray ray = new Ray( transform.position + new Vector3( UnityEngine.Random.Range( -4.0f, 4.0f ), 100.0f, UnityEngine.Random.Range( -4.0f, 4.0f ) ), Vector3.down );
                RaycastHit info;
                if( Physics.Raycast( ray, out info, 200.0f ) )
                {
                    GameObject damageFX = Instantiate( DamageIndicators.Random() );
                    damageFX.transform.position = info.point;
                    activeDamageIndicators.Add( damageFX );

                    GameObject damageCreateFX = Instantiate( DamageCreateIndicators.Random() );
                    damageCreateFX.transform.position = info.point;
                }
            }
            else if( activeDamageIndicators.Count > numIndicators )
            {
                Destroy( activeDamageIndicators[0] );
                activeDamageIndicators.RemoveAt( 0 );
            }
        }
    }


    public void AddNPCAsChild( NPCAI npc )
    {
        if( npcContainer == null )
        {
            npcContainer = new GameObject( "NPCContainer" ).transform;
            npcContainer.parent = transform;
            npcContainer.localPosition = Vector3.zero;
        }

        npc.transform.SetParent( npcContainer );
    }

    public NPCAI[] GetStructureNPCs()
    {
        if( npcContainer == null )
            return new NPCAI[0];

        return npcContainer.GetComponentsInChildren<NPCAI>();
    }

    public void EnableResourceEfficiencyUpdate()
    {
        ResourceEfficiencyUpdateEnabled = true;
        UpdateBuildingEfficiencyMultiplier();
        EnableOnNearBuildingListener();
    }

    public void EnableOverpopulationUpdate()
    {
        OverpopulationUpdateEnabled = true;
        UpdateOverpopulationModifier();
        EnableOnNearBuildingListener();
    }

    private void EnableOnNearBuildingListener()
    {
        CircleFillIterator iterator = new CircleFillIterator( TilePosition, ResourceEfficiencyRadius );
        foreach( Vector2i tile in iterator )
        {
            GameController.Instance.LevelData.AddTileEventListener( tile, OnNearBuildingUpdate );
        }
    }

    public void DisableOnNearBuildingListener()
    {
        CircleFillIterator iterator = new CircleFillIterator( TilePosition, ResourceEfficiencyRadius );
        foreach( Vector2i tile in iterator )
        {
            GameController.Instance.LevelData.RemoveTileEventListener( tile, OnNearBuildingUpdate );
        }
    }

    public void OnNearBuildingUpdate( Vector2i tilePosition )
    {
        if( ResourceEfficiencyUpdateEnabled )
            UpdateBuildingEfficiencyMultiplier();
        else
            UpdateOverpopulationModifier();
    }

    public void UpdateBuildingEfficiencyMultiplier()
    {
        int numPeopleNear = GetNumPeopleNearCircle( ResourceEfficiencyRadius, true );

        ResourceEfficiency = 1.0f + numPeopleNear * 0.25f;
    }

    public void UpdateOverpopulationModifier()
    {
        int livingNear = GetNumPeopleNearCircle( 3, false ) - 1;

        overpopulationModifier = 1.0f + livingNear * OverpopulationAngerModifier;
    }

    public int GetNumPeopleNearCircle( int radius, bool countLivingSpaceLayers )
    {
        GameController controller = GameController.Instance;

        int N = 0;
        CircleFillIterator iterator = new CircleFillIterator( TilePosition, radius );
        foreach( Vector2i tile in iterator )
        {
            Structure structure = controller.LevelData.GetStructure( tile );

            if( structure == null || tile == TilePosition )
                continue;

            if( structure.GetComponent<Slum>() )
            {
                N++;
            }
            else if( structure.GetComponent<LivingSpace>() )
            {
                if( countLivingSpaceLayers )
                    N += structure.GetComponent<LivingSpace>().NumLayers;
                else
                    N++;
            }
        }

        return N;
    }


    public bool FindNewExpansionTile( out Vector2i tile )
    {
        Vector2i start = TilePosition;

        GameController controller = GameController.Instance;

        FloodFillIterator2D iterator = new FloodFillIterator2D( start, ( Vector2i index ) =>
        {
            return !controller.LevelData.IsEmpty( index );
        } );

        List<Vector2i> open = new List<Vector2i>();
        foreach( Vector2i samplePosition in iterator )
        {
            for( int i = 0; i < 4; ++i )
            {
                Vector2i neighbor = samplePosition + Vector2i.Neighbors[i];
                if( controller.LevelData.IsEmpty( neighbor ) )
                {
                    if( !open.Contains( neighbor ) )
                        open.Add( neighbor );
                }
            }
        }

        tile = Vector2i.zero;
        if( open.Count == 0 )
            return false;

        Vector2i target = open.Random();
        tile = target;
        return true;
    }

    public void SpawnNPC( int maximumNPCs )
    {
        GameController controller = GameController.Instance;

        if( catharsis > 0.0f )
            return;

        if( GetStructureNumNPCs() < maximumNPCs )
        {
            Vector3 worldCenter = controller.TileToWorldCentered( TilePosition );

            GameObject NPC = Instantiate( NPCPrefab );
            NPCAI NPCAI = NPC.GetComponent<NPCAI>();
            AddNPCAsChild( NPCAI );
            NPC.transform.position = worldCenter + NPCAI.GetRandomLocalBorderPosition();
        }

        if( IsExpansionEnabled )
        {
            NPCAI[] npcs = GetStructureNPCs();
            if( npcs.Length == maximumNPCs )
            {
                Vector2i newSlumTarget;
                bool newTileFound = FindNewExpansionTile( out newSlumTarget );

                if( !newTileFound )
                {
                    // TODO: Become angry 
                }
                else
                {
                    Structure newSlum = controller.LevelData.CreateBlueprint( StructureType.NewSlum, newSlumTarget );

                    if( newSlum != null )
                    {
                        int detachAmount = npcs.Length / 2;
                        for( int i = 0; i < detachAmount; ++i )
                        {
                            npcs[i].MoveToNewSlum( newSlum );
                        }
                    }
                }
            }
        }
    }

    public void TickAnger( int extraNPCs, int maximumNPCs, float livingSpaceModifier )
    {
        if( catharsis > 0.0f )
        {
            catharsis -= Time.deltaTime;

            MinimapObject.GetComponent<SpriteRenderer>().color = Color.Lerp( Color.white, Color.red, 0.25f + Mathf.PingPong( Time.time, 0.75f ) );

            return;
        }

        int physicalNPCs = GetStructureNumNPCs();
        int actualNPCs = physicalNPCs + extraNPCs;

        float angerFactor = Mathf.Clamp01( ( (float)physicalNPCs / (float)( PopPerLayer / 2.0f ) ) * livingSpaceModifier );

        float resourceAnger = Mathf.SmoothStep( 0.0f, AngerPerS * 2.0f, Mathf.Clamp01( -GameController.Instance.ResourceShortage ) );

        Anger += ( ( AngerPerS + resourceAnger ) * ( overpopulationModifier * angerFactor ) - AngerDecayS ) * Time.deltaTime;

        Anger = Mathf.Clamp01( Anger );

        if( Anger > 0.95f )
        {
            Vector2i attack = TilePosition + new Vector2i( UnityEngine.Random.Range( -2, 2 ), UnityEngine.Random.Range( -2, 2 ) );

            if( attack != TilePosition && !GameController.Instance.LevelData.IsEmpty( attack ) )
            {
                Anger = 0.0f;
                catharsis = 60.0f;

                NPCAI[] npcs = GetStructureNPCs();
                foreach( NPCAI npc in npcs )
                {
                    npc.Riot( attack );
                }
            }
        }

        if( MinimapObject != null )
        {
            Color c = Color.Lerp( Color.white, Color.red, Anger );
            MinimapObject.GetComponent<SpriteRenderer>().color = c;
        }
    }
}
