﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( 20 )]
[RequireComponent( typeof( Structure ) )]
public class AirFilter : MonoBehaviour
{
    public float ProductionSpeed;

    public StructureConsole Console;

    private Structure structure;

    private float lastProduction;


    public static float TotalProduction { get; set; }

    private void OnDestroy()
    {
        TotalProduction -= lastProduction;
    }

    void Start()
    {
        structure = GetComponent<Structure>();
        structure.EnableResourceEfficiencyUpdate();

        if( Console != null )
            InvokeRepeating( "UpdateConsole", 0, 1.0f );
    }

    void FixedUpdate()
    {
        GameController controller = GameController.Instance;

        TotalProduction -= lastProduction;
        lastProduction = GetProdcutionValue();
        TotalProduction += lastProduction;
        
        controller.OxygenPercentage = Mathf.Min( controller.OxygenPercentage + lastProduction * Time.deltaTime, 100.0f );
    }

    float GetProdcutionValue()
    {
        return ProductionSpeed * structure.ResourceEfficiency;
    }

    void UpdateConsole()
    {
        Console.Content = string.Format( "production: {0} oxygen/s\neffeciency: {1}%", GetProdcutionValue().ToString( "0.00" ), ( structure.ResourceEfficiency * 100.0f ).ToString( "0" ) );
    }
}
