using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcePool : Interactable
{
    public UIResourceEfficienyBar efficiencyBar;
    public float AccumulationSpeed;
    public float AccumulationPerHit;
    public float MinAccumulationSpeed;
    public float MaxAccumulationSpeed;
    public float AccumulationDecayPerSecond;

    public float AccumulatedAmount;
    public float NormalizedAccumulationSpeed;


    public ResourceInteractable Container;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ( AccumulationSpeed > MinAccumulationSpeed )
        {
            AccumulationSpeed -= ( AccumulationDecayPerSecond * Time.deltaTime );
        }
        else
        {
            AccumulationSpeed = MinAccumulationSpeed;
        }

        AccumulatedAmount += AccumulationSpeed * Time.deltaTime;

        NormalizedAccumulationSpeed = AccumulationSpeed / MaxAccumulationSpeed;
                
        if( AccumulatedAmount > 1.0f )
        {
            AccumulatedAmount -= 1.0f;
            Container.Resource.Amount++;
        }

        if ( efficiencyBar != null )
        {
            efficiencyBar.SetEfficiency( NormalizedAccumulationSpeed );
            efficiencyBar.TextField.text = string.Format( "Amount of {1} accumulated: {0:N1}", Container.Resource.Amount + AccumulatedAmount, System.Enum.GetName( typeof( Resources ), Container.Resource.type ) );
        }
        
    }


    public override void Interact()
    {
        AccumulationSpeed += ( AccumulationPerHit * MaxAccumulationSpeed );

        if ( AccumulationSpeed > MaxAccumulationSpeed )
        {
            AccumulationSpeed = MaxAccumulationSpeed;
        }
    }
}
